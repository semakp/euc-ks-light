﻿#include "ws2812out.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_tim.h"
#include "stm32f0xx_dma.h"

PUBLIC void Ws2812Out_InitWithDma(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Pin = WS2812OUTFL_PIN | WS2812OUTFR_PIN | WS2812OUTBL_PIN | WS2812OUTBR_PIN;
	GPIO_Init(WS2812OUT_PORT, &GPIO_InitStruct);

	GPIO_PinAFConfig(WS2812OUT_PORT, WS2812OUTFL_PINSOURCE, WS2812OUT_AF);
	GPIO_PinAFConfig(WS2812OUT_PORT, WS2812OUTFR_PINSOURCE, WS2812OUT_AF);
	GPIO_PinAFConfig(WS2812OUT_PORT, WS2812OUTBL_PINSOURCE, WS2812OUT_AF);
	GPIO_PinAFConfig(WS2812OUT_PORT, WS2812OUTBR_PINSOURCE, WS2812OUT_AF);

	///

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 0;
	TIM_TimeBaseInitStruct.TIM_Period = WS2812OUT_TIM_PERIOD;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(WS2812OUT_TIMER, &TIM_TimeBaseInitStruct);
	TIM_ARRPreloadConfig(WS2812OUT_TIMER, ENABLE);

	///

	TIM_OCInitTypeDef TIM_OCInitStruct;
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStruct.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM_OCInitStruct.TIM_OutputNState = TIM_OutputNState_Disable;
	TIM_OCInitStruct.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	TIM_OCInitStruct.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OCInitStruct.TIM_Pulse = 0;
	WS2812OUTFL_TIMER_OCINIT(WS2812OUT_TIMER, &TIM_OCInitStruct);
	WS2812OUTFR_TIMER_OCINIT(WS2812OUT_TIMER, &TIM_OCInitStruct);
	WS2812OUTBL_TIMER_OCINIT(WS2812OUT_TIMER, &TIM_OCInitStruct);
	WS2812OUTBR_TIMER_OCINIT(WS2812OUT_TIMER, &TIM_OCInitStruct);

#if ((WS2812OUT_TIMER_N == 1) || (WS2812OUT_TIMER_N == 15) || (WS2812OUT_TIMER_N == 16) || (WS2812OUT_TIMER_N == 17))
		TIM_CtrlPWMOutputs(ws2812Out->TIMx, ENABLE);
#endif
}

PUBLIC void Ws2812Out_DmaSend(uint8_t (*dmaBuffers)[WS2812OUT__DMA_BUFFER_SIZE])
{
	DMA_InitTypeDef DMA_InitStruct;
	DMA_InitStruct.DMA_BufferSize = WS2812OUT__DMA_BUFFER_SIZE;
	DMA_InitStruct.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStruct.DMA_M2M = DMA_M2M_Disable;
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_Priority = DMA_Priority_VeryHigh;

	// 0
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)dmaBuffers[WS2812OUT_FL];
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&WS2812OUTFL_DMA_PERIPH_ADDR;
	DMA_Init(WS2812OUT_DMA_CHANNEL, &DMA_InitStruct);

	TIM_DMACmd(WS2812OUT_TIMER, TIM_DMA_Update, ENABLE);
	DMA_ClearFlag(WS2812OUT_DMA_FLAG_TC);
	DMA_Cmd(WS2812OUT_DMA_CHANNEL, ENABLE);
	TIM_Cmd(WS2812OUT_TIMER, ENABLE);
	while (!DMA_GetFlagStatus(WS2812OUT_DMA_FLAG_TC)) {}
	DMA_Cmd(WS2812OUT_DMA_CHANNEL, DISABLE);
	TIM_Cmd(WS2812OUT_TIMER, DISABLE);
	TIM_DMACmd(WS2812OUT_TIMER, TIM_DMA_Update, DISABLE);

	// 1
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)dmaBuffers[WS2812OUT_FR];
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&WS2812OUTFR_DMA_PERIPH_ADDR;
	DMA_Init(WS2812OUT_DMA_CHANNEL, &DMA_InitStruct);

	TIM_DMACmd(WS2812OUT_TIMER, TIM_DMA_Update, ENABLE);
	DMA_ClearFlag(WS2812OUT_DMA_FLAG_TC);
	DMA_Cmd(WS2812OUT_DMA_CHANNEL, ENABLE);
	TIM_Cmd(WS2812OUT_TIMER, ENABLE);
	while (!DMA_GetFlagStatus(WS2812OUT_DMA_FLAG_TC)) {}
	DMA_Cmd(WS2812OUT_DMA_CHANNEL, DISABLE);
	TIM_Cmd(WS2812OUT_TIMER, DISABLE);
	TIM_DMACmd(WS2812OUT_TIMER, TIM_DMA_Update, DISABLE);

	// 2
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)dmaBuffers[WS2812OUT_BL];
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&WS2812OUTBL_DMA_PERIPH_ADDR;
	DMA_Init(WS2812OUT_DMA_CHANNEL, &DMA_InitStruct);

	TIM_DMACmd(WS2812OUT_TIMER, TIM_DMA_Update, ENABLE);
	DMA_ClearFlag(WS2812OUT_DMA_FLAG_TC);
	DMA_Cmd(WS2812OUT_DMA_CHANNEL, ENABLE);
	TIM_Cmd(WS2812OUT_TIMER, ENABLE);
	while (!DMA_GetFlagStatus(WS2812OUT_DMA_FLAG_TC)) {}
	DMA_Cmd(WS2812OUT_DMA_CHANNEL, DISABLE);
	TIM_Cmd(WS2812OUT_TIMER, DISABLE);
	TIM_DMACmd(WS2812OUT_TIMER, TIM_DMA_Update, DISABLE);

	// 3
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)dmaBuffers[WS2812OUT_BR];
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&WS2812OUTBR_DMA_PERIPH_ADDR;
	DMA_Init(WS2812OUT_DMA_CHANNEL, &DMA_InitStruct);

	TIM_DMACmd(WS2812OUT_TIMER, TIM_DMA_Update, ENABLE);
	DMA_ClearFlag(WS2812OUT_DMA_FLAG_TC);
	DMA_Cmd(WS2812OUT_DMA_CHANNEL, ENABLE);
	TIM_Cmd(WS2812OUT_TIMER, ENABLE);
	while (!DMA_GetFlagStatus(WS2812OUT_DMA_FLAG_TC)) {}
	DMA_Cmd(WS2812OUT_DMA_CHANNEL, DISABLE);
	TIM_Cmd(WS2812OUT_TIMER, DISABLE);
	TIM_DMACmd(WS2812OUT_TIMER, TIM_DMA_Update, DISABLE);
}

PUBLIC void Ws2812Out_FromBuffer(uint8_t *dmaBuffer, uint32_t *buffer, uint16_t bufSize)
{
	uint16_t i;
	for (i = 0; i < PIXELS; i++)
	{
		uint32_t grb = 0;
		if (i < bufSize)
#ifdef USE_RGB
			grb = CONVERT_RGB_GRB(buffer[i]);
#else
			grb = buffer[i];
#endif

		uint32_t mask;
		uint8_t j;
		for (j = 0, mask = 0x800000; mask; j++, mask >>= 1)
			dmaBuffer[1 + i * 24 + j] = grb & mask
				? WS2812OUT_TIM_CODE1
				: WS2812OUT_TIM_CODE0;
	}
}
