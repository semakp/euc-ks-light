﻿#include "common.h"

PUBLIC void memcpy_u16(uint8_t *target, uint16_t src)
{
	target[0] = (uint8_t)(src>>0);
	target[1] = (uint8_t)(src>>8);
}

PUBLIC void memcpy_u32(uint8_t *target, uint32_t src)
{
	target[0] = (uint8_t)(src>> 0);
	target[1] = (uint8_t)(src>> 8);
	target[2] = (uint8_t)(src>>16);
	target[3] = (uint8_t)(src>>24);
}

PUBLIC void memrcpy(void *dst, void *src, uint16_t len)
{
	uint16_t i;
	for(i=0;i<len;i++)
	{
		((uint8_t *)dst)[i]=((uint8_t *)src)[(len-1)-i];
	}
}

PUBLIC void memrcpy_u16(uint8_t *target, uint16_t src)
{
	target[1] = (uint8_t)(src>>0);
	target[0] = (uint8_t)(src>>8);
}

PUBLIC void memrcpy_u32(uint8_t *target, uint32_t src)
{
	target[3] = (uint8_t)(src>> 0);
	target[2] = (uint8_t)(src>> 8);
	target[1] = (uint8_t)(src>>16);
	target[0] = (uint8_t)(src>>24);
}

PUBLIC uint32_t memrget_u32(uint8_t *src)
{
	return ((uint32_t)src[0] << 24) + ((uint32_t)src[1] << 16) + ((uint32_t)src[2] << 8) + (src[3]);
}

PUBLIC uint16_t memrget_u16(uint8_t *src)
{
	return ((uint16_t)src[0] << 8) + (src[1]);
}

PUBLIC uint16_t memget_u16(uint8_t *src)
{
	return ((uint16_t)src[1] << 8) + (src[0]);
}

PUBLIC void delay_us(unsigned long n)
{
	volatile unsigned long i;

	while(n--)
	{
		i=2;
		while(i--);
	}
}

PUBLIC void delay_ms(unsigned long n)
{
	while(n--)
	{
		delay_us(1000);
	}
}


PUBLIC void Common_sendStr(ACTION_DATA(send), char *str)
{
	send((uint8_t *)str, strlen(str));
}

PUBLIC void Common_sendDec(ACTION_DATA(send), uint16_t num, uint8_t dec)
{
	uint8_t buf[5];
	uint8_t i;
	uint16_t div;
	for(i=0,div=10000; div>0; div/=10, i++){
		buf[i] = num / div + '0';
		num %= div;
	}
	send(buf + 5 - dec, dec);
}

/**
 * Отправить 32-разрядное число через делегат.
 * send - Делегат.
 * num - Число для отправки.
 * dec - Количество числовых символов для отображения (не включая '.').
 * space - Символ пустого места (для незначащих нулей).
 * dot - Фиксированная точка. 0 - без точки.
 */
PUBLIC void Common_sendDec32(ACTION_DATA(send), uint32_t num, uint8_t dec, char space, uint8_t dot)
{
	if (dot > 10)
		dot = 10;
	if (dot > dec)
		dec = dot;
	dot = 10 - dot;

	uint8_t buf[11];
	uint32_t div;
	uint8_t isSpace = 1;

	uint8_t i;
	for(i = 0, div = 1000000000UL; div > 0; div /= 10, i++) {
		uint8_t digit = num / div;
		if (digit != 0 || !isSpace || div == 1 || i >= dot)
		{
			if (i >= dot)
			{
				if (i == dot)
					buf[i] = '.';
				buf[i + 1] = digit + '0';
			}
			else
				buf[i] = digit + '0';
			isSpace = 0;
		}
		else
		{
			buf[i] = space;
		}
		num %= div;
	}

	if (dot == 10)
		send(buf + 10 - dec, dec);
	else
		send(buf + 10 - dec, dec + 1);
}

PUBLIC void Common_sendHex(ACTION_DATA(send), uint32_t num, uint8_t dec)
{
	uint8_t buf[8];
	uint8_t i;
	for(i=8; i>0; i--)
	{
		uint8_t hex = num & 0x0f;
		buf[i-1] = (hex >= 10)? (hex - 10) + 'A' : hex + '0';
		num >>= 4;
	}
	send(buf + 8 - dec, dec);
}

PUBLIC uint16_t Common_memFind(void *psrc, uint16_t srclen, void *pdst, uint16_t dstlen)
{
	if((srclen > 0) && (dstlen > 0) && (dstlen <= srclen))
	{
		uint8_t *pu8Src = (uint8_t *)psrc;
		uint8_t *pu8Dst = (uint8_t *)pdst;
		uint16_t i, j;
		for(i=0; i<(srclen - dstlen); i++)
		{
			if(pu8Src[i] == pu8Dst[0])
			{
				for(j=1; j<dstlen; j++)
				{
					if(pu8Src[i + j] != pu8Dst[j]) break;
				}

				if(j == dstlen)
				{
					return i;
				}
			}
		}
	}

	return srclen;

}

PUBLIC uint8_t Common_memContains(void *psrc, uint16_t srclen, void *pdst, uint16_t dstlen)
{
	return Common_memFind(psrc, srclen, pdst, dstlen) != srclen;
}

PUBLIC uint8_t Common_Circle_IndexAddWillOverflow(uint16_t inx, uint16_t *outNextInx, uint16_t add, uint16_t maxInx, uint16_t circleSize)
{
	if(add > circleSize)
	{
		return 1;
	}
	else if(inx >= (circleSize - add))
	{
		*outNextInx = inx - (circleSize - add);
		return ((*outNextInx >= maxInx) || (maxInx > inx));
	}
	else
	{
		*outNextInx = inx + add;
		return ((inx < maxInx) && (maxInx <= *outNextInx));
	}
}

PUBLIC uint8_t Common_Circle_IndexIncrementWillOverflow(uint16_t inx, uint16_t *outNextInx, uint16_t maxInx, uint16_t circleSize)
{
	return Common_Circle_IndexAddWillOverflow(inx, outNextInx, 1, maxInx, circleSize);
}
