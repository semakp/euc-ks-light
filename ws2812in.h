﻿#ifndef __WS2812_IN__
#define __WS2812_IN__

#include "stdafx.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_tim.h"
#include "stm32f0xx_dma.h"
#include "hardware.h"

typedef struct
{
	GPIO_TypeDef *GPIO_Port;
	uint32_t GPIO_Pin;
	uint8_t GPIO_PinSource;
	uint8_t GPIO_AF;
	DMA_Channel_TypeDef* DMAy_Channelx;
	uint32_t DMA_PeripheralBaseAddr;
	uint32_t DMA_MemoryBaseAddr;
	uint32_t DMA_Channel_IT_TC;
	uint16_t DMA_BufferSize;
	TIM_TypeDef* TIMx;
	uint32_t TIM_Period;
	uint16_t TIM_Channel;
	uint16_t TIM_InputTriggerSource;
	uint8_t NVIC_IRQChannel_TIM;
	uint8_t NVIC_IRQChannel_DMA;
	uint16_t TIM_DMASource;
	uint16_t dataReady;
	uint8_t isInit;
	uint8_t isBusy;
	uint32_t captures;
	uint32_t hits;
}
Ws2812InStruct_t;

PUBLIC void Ws2812In_StatsPrintAndReset(ACTION_DATA(send), Ws2812InStruct_t *ws2812In);

/**
 * Init struct for WS2812-Front.
 */
PUBLIC void Ws2812In_InitStructFrontFromHardwareH(Ws2812InStruct_t *ws2812in, uint8_t *ws2812In_DmaBuffer);
/**
 * Init struct for WS2812-Back.
 */
PUBLIC void Ws2812In_InitStructBackFromHardwareH(Ws2812InStruct_t *ws2812in, uint8_t *ws2812In_DmaBuffer);

/**
 * Init peripherals for WS2812-Input from init structure.
 */
PUBLIC void Ws2812In_InitWithDmaAndIsr(Ws2812InStruct_t *ws2812In);

/**
 * Start capture for WS2812-Input.
 */
PUBLIC void Ws2812In_Start(Ws2812InStruct_t *ws2812In);

/**
 * Get captured WS2812-Input to pixelbuffer.
 */
PUBLIC void Ws2812In_ToBuffer(uint8_t *dmaInBuffer, uint32_t *buffer, uint16_t bufSize, uint8_t offset);

/**
 * Print raw dmaBuffer thru callback.
 */
PUBLIC void Ws2812In_BufferPrint(ACTION_DATA(send), uint8_t *dmaInBuffer);

/**
 * Irq handler for WS2812-DMA Channel.
 */
PUBLIC void Ws2812In_DmaIrqHandler(Ws2812InStruct_t *ws2812In);

/**
 * Irq handler for WS2812-TIM.
 */
PUBLIC void Ws2812In_TimIrqHandler(Ws2812InStruct_t *ws2812In);

PUBLIC void Ws2812InputsFlushTo(ACTION_DATA(send), uint8_t showLog, uint32_t (*grbBuffers)[PIXELS], Ws2812InStruct_t *inputs, uint8_t (*dmaBuffers)[WS2812IN__DMA_BUFFER_SIZE]);

#endif /* __WS2812_IN__ */
