﻿#ifndef __BOOTLOADER_CLIENT_H__
#define __BOOTLOADER_CLIENT_H__

// settings

#define HW_FLASH_PSTART_PAGE					14
#define BOOTLOADER_CONFIG_PAGE_NUMBER           12

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "stm32f0xx.h"

#define HW_FLASH_POINTER						0x08000000UL
#define HW_FLASH_PAGE_SIZE						0x0400UL
#define HW_FLASH_PSTART_ADDRESS					(HW_FLASH_POINTER + HW_FLASH_PAGE_SIZE * HW_FLASH_PSTART_PAGE)
#define BOOTLOADER_CONFIG_PAGE_ADDRESS         	( (uint32_t)(HW_FLASH_POINTER + HW_FLASH_PAGE_SIZE * BOOTLOADER_CONFIG_PAGE_NUMBER) )

// Флаг принудительного запуска бутлоадера - может выставить любой.
#define BOOTLOADER_CONFIG_BTLKEY_ADDRESS      	( (uint32_t)(HW_FLASH_POINTER + HW_FLASH_PAGE_SIZE * BOOTLOADER_CONFIG_PAGE_NUMBER) )
#define BOOTLOADER_CONFIG_BTLKEY_VALUE         	( *(__IO uint32_t *) BOOTLOADER_CONFIG_BTLKEY_ADDRESS )
#define BOOTLOADER_KEY_VALUE                    (0xAAAA5555UL)

// Флаг "В процессе записи прошивки во флеш" - выставляет бутлоадер.
#define BOOTLOADER_CONFIG_FIP_ADDRESS          	( (uint32_t)(HW_FLASH_POINTER + HW_FLASH_PAGE_SIZE * BOOTLOADER_CONFIG_PAGE_NUMBER + 4) )
#define BOOTLOADER_CONFIG_FIP_VALUE	           	( *(__IO uint32_t *) BOOTLOADER_CONFIG_FIP_ADDRESS )
#define BOOTLOADER_FLASH_IN_PROGRESS            (0xDEAD0DADUL)

// Признак (первого) запуска программы - выставляет бутлоадер.
#define BOOTLOADER_CONFIG_PIS_ADDRESS			( (uint32_t)(HW_FLASH_POINTER + HW_FLASH_PAGE_SIZE * BOOTLOADER_CONFIG_PAGE_NUMBER + 8) )
#define BOOTLOADER_CONFIG_PIS_VALUE	           	( *(__IO uint32_t *) BOOTLOADER_CONFIG_PIS_ADDRESS )
#define BOOTLOADER_PROGRAM_IS_STARTED			(0xA5F0A5FEUL)

// Признак успешного запуска программы - выставляет программа.
#define BOOTLOADER_CONFIG_POK_ADDRESS			( (uint32_t)(HW_FLASH_POINTER + HW_FLASH_PAGE_SIZE * BOOTLOADER_CONFIG_PAGE_NUMBER + 12) )
#define BOOTLOADER_CONFIG_POK_VALUE	           	( *(__IO uint32_t *) BOOTLOADER_CONFIG_POK_ADDRESS )
#define BOOTLOADER_PROGRAM_IS_OK				(0x0C0CC0C0UL)

#define __HW_IS_PROGRAM_EXIST__					( ( ( *(uint32_t *) HW_FLASH_PSTART_ADDRESS ) & 0x2FFF0000 ) == 0x20000000 )
#define BOOTLOADER_IS_FORCED					( BOOTLOADER_CONFIG_BTLKEY_VALUE == BOOTLOADER_KEY_VALUE )
#define BOOTLOADER_IS_FLASH_IN_PROGRESS			( BOOTLOADER_CONFIG_FIP_VALUE == BOOTLOADER_FLASH_IN_PROGRESS )
#define BOOTLOADER_IS_PROGRAM_START_FAILS		( ( BOOTLOADER_CONFIG_PIS_VALUE == BOOTLOADER_PROGRAM_IS_STARTED ) && ( BOOTLOADER_CONFIG_POK_VALUE != BOOTLOADER_PROGRAM_IS_OK ) )

PUBLIC void BootloaderClient_ProgramInit(void);
PUBLIC void BootloaderClient_StartBootloader(uint32_t key);

#endif /* __BOOTLOADER_CLIENT_H__ */
