﻿#ifndef __DS18B20__
#define __DS18B20__

#include "stdafx.h"
#include "stm32f0xx_gpio.h"

#define ONEWIRE_PORT 			GPIOB
#define ONEWIRE_PIN_DS1820_L 	GPIO_Pin_0
#define ONEWIRE_PIN_DS1820_N_L	(0)
#define ONEWIRE_PIN_DS1820_R 	GPIO_Pin_1
#define ONEWIRE_PIN_DS1820_N_R	(1)

PUBLIC void Ds18b20_Init(uint16_t mask);
PUBLIC uint8_t Ds18b20_ConvertTemp(uint16_t mask);
PUBLIC int16_t Ds18b20_GetTemp(uint16_t mask);

#endif /* __DS18B20__ */
