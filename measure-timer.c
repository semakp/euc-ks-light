﻿#include "measure-timer.h"

PUBLIC void MeasureTimer_Init(TIM_TypeDef* TIMx, uint16_t prescaler)
{
	if (prescaler == 0)
		prescaler = 1;

	TIM_ARRPreloadConfig(TIMx, DISABLE);
	TIM_SelectOnePulseMode(TIMx, TIM_OPMode_Single);
	TIM_ITConfig(TIMx, TIM_IT_Update, ENABLE);

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Prescaler = (prescaler - 1);
	TIM_TimeBaseInitStruct.TIM_Period = 0xFFFF;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIMx, &TIM_TimeBaseInitStruct);
}

