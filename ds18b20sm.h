﻿#ifndef __DS18B20__
#define __DS18B20__

#include "stdafx.h"
#include "stm32f0xx_gpio.h"
#include "hardware.h"

#define ONEWIRE_PORT 			GPIOB
#define ONEWIRE_PIN_DS1820_L 	GPIO_Pin_0
#define ONEWIRE_PIN_DS1820_N_L	(0)
#define ONEWIRE_PIN_DS1820_R 	GPIO_Pin_1
#define ONEWIRE_PIN_DS1820_N_R	(1)

#define DS18B20_TEMP_ERROR		9999

typedef struct 
{
	uint16_t enableMask;
	uint16_t presenceMask;
	uint16_t convertedMask;
	uint16_t conversionTicks;
	uint16_t noPresentTicks;
	int16_t temp[16];
}
Ds18b20SM_InitStruct_t;

PUBLIC void Ds18b20SM_Init(Ds18b20SM_InitStruct_t *initStruct);
PUBLIC void Ds18b20SM_Tick(void);
PUBLIC uint8_t Ds18b20SM_InsertNextFromMask(uint16_t *mask);

#endif /* __DS18B20__ */
