﻿#include "ws2812in.h"
#include "common.h"
#include "stm32f0xx_misc.h"

PUBLIC void Ws2812In_InitStructFrontFromHardwareH(Ws2812InStruct_t *ws2812in, uint8_t *ws2812In_DmaBuffer)
{
	ws2812in->GPIO_Port = WS2812INF_PORT;
	ws2812in->GPIO_Pin = WS2812INF_PIN;
	ws2812in->GPIO_PinSource = WS2812INF_PINSOURCE;
	ws2812in->GPIO_AF = WS2812INF_AF;
	ws2812in->DMAy_Channelx = WS2812INF_DMA_CHANNEL;
	ws2812in->DMA_PeripheralBaseAddr = WS2812INF_DMA_PERIPH_ADDR;
	ws2812in->DMA_MemoryBaseAddr = (uint32_t)ws2812In_DmaBuffer;
	ws2812in->DMA_BufferSize = WS2812IN__DMA_BUFFER_SIZE;
	ws2812in->DMA_Channel_IT_TC = WS2812INF_DMA_CHANNEL_IRQ_TC;
	ws2812in->TIMx = WS2812INF_TIMER;
	ws2812in->TIM_Period = WS2812IN_TIM_PERIOD;
	ws2812in->TIM_Channel = WS2812INF_TIMER_CHANNEL;
	ws2812in->TIM_InputTriggerSource = WS2812INF_TIMER_TRG;
	ws2812in->NVIC_IRQChannel_TIM = WS2812INF_TIMER_IRQN;
	ws2812in->NVIC_IRQChannel_DMA = WS2812INF_DMA_IRQN;
	ws2812in->TIM_DMASource = WS2812INF_TIMER_DMASRC;
}

PUBLIC void Ws2812In_InitStructBackFromHardwareH(Ws2812InStruct_t *ws2812in, uint8_t *ws2812In_DmaBuffer)
{
	ws2812in->GPIO_Port = WS2812INB_PORT;
	ws2812in->GPIO_Pin = WS2812INB_PIN;
	ws2812in->GPIO_PinSource = WS2812INB_PINSOURCE;
	ws2812in->GPIO_AF = WS2812INB_AF;
	ws2812in->DMAy_Channelx = WS2812INB_DMA_CHANNEL;
	ws2812in->DMA_PeripheralBaseAddr = WS2812INB_DMA_PERIPH_ADDR;
	ws2812in->DMA_MemoryBaseAddr = (uint32_t)ws2812In_DmaBuffer;
	ws2812in->DMA_BufferSize = WS2812IN__DMA_BUFFER_SIZE;
	ws2812in->DMA_Channel_IT_TC = WS2812INB_DMA_CHANNEL_IRQ_TC;
	ws2812in->TIMx = WS2812INB_TIMER;
	ws2812in->TIM_Period = WS2812IN_TIM_PERIOD;
	ws2812in->TIM_Channel = WS2812INB_TIMER_CHANNEL;
	ws2812in->TIM_InputTriggerSource = WS2812INB_TIMER_TRG;
	ws2812in->NVIC_IRQChannel_TIM = WS2812INB_TIMER_IRQN;
	ws2812in->NVIC_IRQChannel_DMA = WS2812INB_DMA_IRQN;
	ws2812in->TIM_DMASource = WS2812INB_TIMER_DMASRC;
}

PUBLIC void Ws2812In_InitWithDmaAndIsr(Ws2812InStruct_t *ws2812In)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Pin = ws2812In->GPIO_Pin;
	GPIO_Init(ws2812In->GPIO_Port, &GPIO_InitStruct);
	GPIO_PinAFConfig(ws2812In->GPIO_Port, ws2812In->GPIO_PinSource, ws2812In->GPIO_AF);

	///

	DMA_InitTypeDef DMA_InitStruct;
	DMA_InitStruct.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStruct.DMA_M2M = DMA_M2M_Disable;
	DMA_InitStruct.DMA_BufferSize = 0;
	DMA_InitStruct.DMA_MemoryBaseAddr = ws2812In->DMA_MemoryBaseAddr;
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_PeripheralBaseAddr = ws2812In->DMA_PeripheralBaseAddr;
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_Priority = DMA_Priority_Medium;
	DMA_Init(ws2812In->DMAy_Channelx, &DMA_InitStruct);

	DMA_ITConfig(ws2812In->DMAy_Channelx, DMA_IT_TC, ENABLE);

	///

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 0;
	TIM_TimeBaseInitStruct.TIM_Period = ws2812In->TIM_Period;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(ws2812In->TIMx, &TIM_TimeBaseInitStruct);

	TIM_ARRPreloadConfig(ws2812In->TIMx, ENABLE);

	TIM_ICInitTypeDef TIM_ICInitStruct;
	TIM_ICInitStruct.TIM_Channel = ws2812In->TIM_Channel;
	TIM_ICInitStruct.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_ICInitStruct.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStruct.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStruct.TIM_ICFilter = 0;
	TIM_PWMIConfig(ws2812In->TIMx, &TIM_ICInitStruct);

	TIM_SelectInputTrigger(ws2812In->TIMx, ws2812In->TIM_InputTriggerSource);
	TIM_SelectSlaveMode(ws2812In->TIMx, TIM_SlaveMode_Reset);
	TIM_SelectMasterSlaveMode(ws2812In->TIMx, TIM_MasterSlaveMode_Enable);

	TIM_UpdateRequestConfig(ws2812In->TIMx, TIM_UpdateSource_Regular);
	TIM_DMACmd(ws2812In->TIMx, ws2812In->TIM_DMASource, DISABLE);

	///

	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = ws2812In->NVIC_IRQChannel_TIM;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = ws2812In->NVIC_IRQChannel_DMA;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);

	///

	ws2812In->isInit = 1;
	ws2812In->isBusy = 0;
	ws2812In->dataReady = 0;
	ws2812In->captures = 0;
	ws2812In->hits = 0;
}

PUBLIC void Ws2812In_StatsPrintAndReset(ACTION_DATA(send), Ws2812InStruct_t *ws2812In)
{
	Common_sendStr(send, "hits/captures = ");
	Common_sendDec32(send, ws2812In->hits, 10, ' ', 0);
	Common_sendStr(send, "/");
	Common_sendDec32(send, ws2812In->captures, 10, ' ', 0);
	Common_sendStr(send, "=");
	Common_sendDec32(send, (ws2812In->hits * 1000) / ws2812In->captures, 4, ' ', 1);
	Common_sendStr(send, "%\r\n");

	ws2812In->hits = 0;
	ws2812In->captures = 0;
}

PUBLIC void Ws2812In_Start(Ws2812InStruct_t *ws2812In)
{
	if (!ws2812In->isInit || ws2812In->isBusy)
		return;

	ws2812In->isBusy = 1;

    if (ws2812In->dataReady == (PIXELS * 24 + 1) || ws2812In->dataReady == (PIXELS * 24))
    	ws2812In->hits++;
    ws2812In->captures++;

	DMA_SetCurrDataCounter(ws2812In->DMAy_Channelx, ws2812In->DMA_BufferSize);
	DMA_Cmd(ws2812In->DMAy_Channelx, ENABLE);

	TIM_ITConfig(ws2812In->TIMx, TIM_IT_Update, ENABLE);
	TIM_ClearFlag(ws2812In->TIMx, TIM_FLAG_CC1 | TIM_FLAG_CC2);

	TIM_DMACmd(ws2812In->TIMx, ws2812In->TIM_DMASource, ENABLE);
	TIM_Cmd(ws2812In->TIMx, ENABLE);
}

PUBLIC void Ws2812In_ToBuffer(uint8_t *dmaInBuffer, uint32_t *buffer, uint16_t bufSize, uint8_t offset)
{
	uint16_t len = MIN(bufSize, PIXELS);
	uint16_t i;
	for (i = 0; i < len; i++)
	{
		uint32_t grb = 0;
		uint32_t mask;
		uint8_t j;
		for (j = 0, mask = 0x800000; mask; mask >>= 1, j++)
		{
			if (offset == 0xFF)
			{
				if (i == 0 && j == 0)
					grb |= (mask & buffer[i]);
				else if (dmaInBuffer[i * 24 + j - 1] >= WS2812IN_TIM_BORDER)
					grb |= mask;
			}
			else if (dmaInBuffer[i * 24 + j + offset] >= WS2812IN_TIM_BORDER)
				grb |= mask;
		}

#ifdef USE_RGB
		buffer[i] = CONVERT_RGB_GRB(grb);
#else
    	buffer[i] = grb;
#endif
	}
}

PUBLIC void Ws2812In_BufferPrint(ACTION_DATA(send), uint8_t *dmaInBuffer)
{
	uint16_t i;
	for (i = 0; i < PIXELS; i++)
	{
		uint8_t j;
		for (j = 0; j < 24; j++)
		{
			uint16_t value = dmaInBuffer[i * 24 + j];
			if (value < 7)
			{
				Common_sendHex(send, value, 1);
				Common_sendStr(send, ",");
			}
			else if (value < 0x13)
			{
				Common_sendStr(send, "-");
			}
			else if (value < 0x1A)
			{
				Common_sendHex(send, value, 2);
				Common_sendStr(send, ",");
			}
			else if (value < 0x30)//was 0x27)
			{
				Common_sendStr(send, "+");
			}
			else if (value < 0x100)
			{
				Common_sendHex(send, value, 2);
				Common_sendStr(send, ",");
			}
			else
			{
				Common_sendHex(send, value, 3);
				Common_sendStr(send, ",");
			}
		}
    	Common_sendStr(send, "\r\n");
	}
}

PUBLIC inline void Ws2812In_DmaIrqHandler(Ws2812InStruct_t *ws2812In)
{
	if (ws2812In->isBusy && DMA_GetITStatus(ws2812In->DMA_Channel_IT_TC))
	{
		DMA_ClearITPendingBit(ws2812In->DMA_Channel_IT_TC);
		TIM_ITConfig(ws2812In->TIMx, TIM_IT_Update, DISABLE);
    	DMA_Cmd(ws2812In->DMAy_Channelx, DISABLE);
		TIM_DMACmd(ws2812In->TIMx, ws2812In->TIM_DMASource, DISABLE);
		TIM_Cmd(ws2812In->TIMx, DISABLE);

		ws2812In->dataReady = ws2812In->DMA_BufferSize - DMA_GetCurrDataCounter(ws2812In->DMAy_Channelx);
    	ws2812In->isBusy = 0;
	}
}

PUBLIC inline void Ws2812In_TimIrqHandler(Ws2812InStruct_t *ws2812In)
{
	if (ws2812In->isBusy && TIM_GetITStatus(ws2812In->TIMx, TIM_IT_Update))
	{
		TIM_ClearITPendingBit(ws2812In->TIMx, TIM_IT_Update);
		ws2812In->dataReady = ws2812In->DMA_BufferSize - DMA_GetCurrDataCounter(ws2812In->DMAy_Channelx);

		if (ws2812In->dataReady < (PIXELS * 24))
		{
			DMA_Cmd(ws2812In->DMAy_Channelx, DISABLE);
			DMA_SetCurrDataCounter(ws2812In->DMAy_Channelx, ws2812In->DMA_BufferSize);
			DMA_Cmd(ws2812In->DMAy_Channelx, ENABLE);

			TIM_DMACmd(ws2812In->TIMx, ws2812In->TIM_DMASource, ENABLE);
			return;
		}

		TIM_ITConfig(ws2812In->TIMx, TIM_IT_Update, DISABLE);
		DMA_Cmd(ws2812In->DMAy_Channelx, DISABLE);
		TIM_DMACmd(ws2812In->TIMx, ws2812In->TIM_DMASource, DISABLE);
		TIM_Cmd(ws2812In->TIMx, DISABLE);

    	ws2812In->isBusy = 0;
	}
}

PUBLIC void Ws2812InputsFlushTo(ACTION_DATA(serialSendData), uint8_t showLog, uint32_t (*grbBuffers)[PIXELS], Ws2812InStruct_t *inputs, uint8_t (*dmaBuffers)[WS2812IN__DMA_BUFFER_SIZE])
{
	uint8_t i;
    for (i = 0; i < WS2812IN_TOTAL; i++)
    {
    	uint16_t dataReady = inputs[i].dataReady;
        if (inputs[i].isInit && !inputs[i].isBusy)
        {
        	if (showLog > 0)
        	{
        		Common_sendStr(serialSendData, "i=");
        		Common_sendDec(serialSendData, i, 1);
        		Common_sendStr(serialSendData, " dataReady=");
        		Common_sendDec(serialSendData, inputs[i].dataReady, 5);
        		Common_sendStr(serialSendData, "\r\n");
        		Ws2812In_BufferPrint(serialSendData, dmaBuffers[i]);
        	}
            if (dataReady == WS2812IN_DATAREADY_OK)
            {
            	Ws2812In_ToBuffer(dmaBuffers[i], grbBuffers[i], PIXELS, 0);
            }
            else if (dataReady == (PIXELS * 24))
            {
            	Ws2812In_ToBuffer(dmaBuffers[i], grbBuffers[i], PIXELS, 0);
            }
            else
            {
            	if (showLog > 0)
            	{
					Common_sendStr(serialSendData, "\r\nDataReady[");
					Common_sendDec(serialSendData, i, 1);
					Common_sendStr(serialSendData, "]=");
					Common_sendDec(serialSendData, inputs[i].dataReady, 5);
					Common_sendStr(serialSendData, "\r\n");
            	}
    			Ws2812In_BufferPrint(serialSendData, dmaBuffers[i]);
            }
        	Ws2812In_Start(&inputs[i]);
        }
    }
}
