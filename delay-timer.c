﻿#include "delay-timer.h"

PRIVATE void _delayInit(uint8_t isDelayMs);
PRIVATE uint8_t isStarted;

/**
 * Инициализация таймера для установки задержек.
 */
PUBLIC void DelayTimer_Init(void)
{
	TIM_ARRPreloadConfig(DELAY_TIMER, DISABLE);
	TIM_SelectOnePulseMode(DELAY_TIMER, TIM_OPMode_Single);
	_delayInit(0);
}

/**
 * Установка задержки в миллисекундах.
 */
PUBLIC void DelayTimer_ms(uint16_t delayTime)
{
	if (delayTime < 2)
	{
		DELAYTIMER_US(delayTime * 1000);
		return;
	}

	delayTime <<= 1;

	_delayInit(1);
	TIM_SetAutoreload(DELAY_TIMER, delayTime - 1);
	TIM_ClearFlag(DELAY_TIMER, TIM_FLAG_Update);
	TIM_Cmd(DELAY_TIMER, ENABLE);
	while (!TIM_GetFlagStatus(DELAY_TIMER, TIM_FLAG_Update)) {}
	_delayInit(0);
}

/**
 * Установка задержки в микросекундах. Минимум - 6мкс.
 */
PUBLIC void DelayTimer_us(uint16_t delayTime)
{
	DELAY_TIMER->ARR = delayTime - 5;
	DELAY_TIMER->SR = (uint16_t)~TIM_FLAG_Update;
	DELAY_TIMER->CR1 |= TIM_CR1_CEN;
	while (!(DELAY_TIMER->SR & TIM_FLAG_Update)) {}
}

PUBLIC uint8_t DelayTimer_timerMs(uint16_t delayTime)
{
	if (isStarted && !TIM_GetFlagStatus(DELAY_TIMER, TIM_FLAG_Update))
		return 0;

	if (delayTime < 2)
		delayTime = 1;
	else
		delayTime = (delayTime << 1) - 1;

	_delayInit(1);
	isStarted = 1;
	TIM_SetAutoreload(DELAY_TIMER, delayTime);
	TIM_ClearFlag(DELAY_TIMER, TIM_FLAG_Update);
	TIM_Cmd(DELAY_TIMER, ENABLE);
	return 1;
}

PRIVATE void _delayInit(uint8_t isDelayMs)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Prescaler = isDelayMs ? (24000 - 1) : (48 - 1);
	TIM_TimeBaseInitStruct.TIM_Period = 0;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(DELAY_TIMER, &TIM_TimeBaseInitStruct);
	isStarted = 0;
}

/*
 // TEST

	uint16_t i;
	for (i = 5; i < 15; i++)
	{
		MEASURE_TIMER_START;
		DELAYTIMER_US(i);
		MEASURE_TIMER_STOP(measured);
		ledSet(1);

		Common_sendStr(serialSendData, "delay set=");
		Common_sendDec(serialSendData, i, 2);
		Common_sendStr(serialSendData, "us measured=");
		Common_sendDec32(serialSendData, measured * 125, 10, '_', 3);
		Common_sendStr(serialSendData, "us\r\n");
	}

	for (i = 6; i < 16; i++)
	{
		MEASURE_TIMER_START;
		DelayTimer_us(i);
		MEASURE_TIMER_STOP(measured);
		ledSet(1);

		Common_sendStr(serialSendData, "delay set=");
		Common_sendDec(serialSendData, i, 2);
		Common_sendStr(serialSendData, "us measured=");
		Common_sendDec32(serialSendData, measured * 125, 10, '_', 3);
		Common_sendStr(serialSendData, "us\r\n");
	}

 */



