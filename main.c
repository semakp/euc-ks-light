﻿#include "stdafx.h"
#include "bootloader-client.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_usart.h"
#include "hardware.h"
#include "common.h"
#include "measure-timer.h"
#include "delay-timer.h"
#include "ds18b20sm.h"
#include "ws2812in.h"
#include "ws2812out.h"
#include "ks-light.h"

/////////////////////////////////////////////

#define SERIAL_INITIALIZE		1
#define TICK_MS					10
#define DS18B20_NOPRESENT_TICKS	(500 / TICK_MS)
#define DS18B20_CONV_TICKS		(750 / TICK_MS)
#define KSLIGHT_TEMPER_TICKS	(5000 / TICK_MS)
#define KSLIGHT_PERIOD_TICKS	(10000 / TICK_MS)
#define TIMER1SEC_TICKS			(1000 / TICK_MS)
#define KSLIGHT_BATTERYMODE_DELAY_TO_OFF	(5000 / TICK_MS)

#define MEASURE_TIMER_US	TIMER_FOR_MEASURE_US
#define MEASURE_TIMER_MS	TIMER_FOR_MEASURE_MS

#define MEASURE_US_IN		MEASURE_TIMER_START(MEASURE_TIMER_US)
#define MEASURE_US_OUT(x)	MEASURE_TIMER_STOP(MEASURE_TIMER_US, (x))
#define MEASURE_MS_IN		MEASURE_TIMER_START(MEASURE_TIMER_MS)
#define MEASURE_MS_OUT(x)	MEASURE_TIMER_STOP(MEASURE_TIMER_MS, (x))





#define BOOTLOADER				BOOTLOADER

#define DS18B20_TOTAL	2
#define DS18B20_L		0
#define DS18B20_R		1

PRIVATE uint8_t ws2812In_DmaBuffers[WS2812IN_TOTAL][WS2812IN__DMA_BUFFER_SIZE];
PRIVATE uint8_t ws2812Out_DmaBuffers[WS2812OUT_TOTAL][WS2812OUT__DMA_BUFFER_SIZE];
PRIVATE uint32_t ws2812in_GrbBuffers[WS2812IN_TOTAL][PIXELS];

#define MEASURE_PRESCALER_US	1
#define MEASURED_PRINT_US(send, measured) do{Common_sendDec32(send, (measured) * 2083 / 100, 10, '_', 3);}while(0)

#define MEASURE_PRESCALER_MS	1000
#define MEASURED_PRINT_MS(send, measured) do{Common_sendDec32(send, (measured) * 2083 / 100, 10, '_', 3);}while(0)

#define SERIAL_USART	USART1
#define SERIAL_PORT		GPIOA
#define SERIAL_PINS		(GPIO_Pin_9 | GPIO_Pin_10)
#define SERIAL_PSRC0	GPIO_PinSource9
#define SERIAL_PSRC1	GPIO_PinSource10
#define SERIAL_AF		GPIO_AF_1
//#define SERIAL_DMA_TX	DMA_CHANNEL_FOR_SERIAL_TX
//#define SERIAL_DMA_TX_TC DMA1_FLAG_TC2
//#define SERIAL_DMA_TX_IS_MAPPED_TO_CHANNEL4 DISABLE

const uint32_t uart1BaudRate = 115200;

#define SERIAL_TX_BUFFER_SIZE	2048

PRIVATE uint8_t serialTxBuffer[SERIAL_TX_BUFFER_SIZE];

#define PRINT_BUFFER_LENGTH_MAX 2048
PRIVATE uint16_t printBufferLength;
PRIVATE uint8_t printBuffer[PRINT_BUFFER_LENGTH_MAX];

PRIVATE uint8_t serialIsInitialized;

Ws2812InStruct_t ws2812in[WS2812IN_TOTAL];
Ds18b20SM_InitStruct_t ds18b20;

uint8_t ws2812out_isInit;
uint8_t ws2812out_isBusy;

PRIVATE uint32_t temp_GrbBuffers[DS18B20_TOTAL][PIXELS];

/////////////////////////////////////////////

PRIVATE void chipReset(void)
{
	RCC_HSICmd(ENABLE);
	while (!RCC_GetFlagStatus(RCC_FLAG_HSIRDY)) {}

	//48 MHz
	RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_12);
	RCC_PLLCmd(ENABLE);

	while (!RCC_GetFlagStatus(RCC_FLAG_PLLRDY)) {}
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	RCC_PCLKConfig(RCC_HCLK_Div1);

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_DMA1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM3 | RCC_APB1Periph_TIM14, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_TIM15 | RCC_APB2Periph_TIM16 | RCC_APB2Periph_TIM17, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_StructInit(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_All;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    serialIsInitialized = 0;
}


// PB8 is Button
PRIVATE void buttonInit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
}

PRIVATE uint8_t buttonIsPressed(void)
{
	return GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_8) == 0;
}

PRIVATE void waitForButtonPress(void)
{
	while (!buttonIsPressed()) {}
	while (buttonIsPressed()) {}
}

// PB9 is LED
PRIVATE void ledInit(BitAction bitState)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	GPIO_WriteBit(GPIOB, GPIO_Pin_9, bitState);
}

PRIVATE void ledSet(BitAction bitState)
{
	GPIO_WriteBit(GPIOB, GPIO_Pin_9, bitState);
}

#if 1
PRIVATE void initSerial(uint32_t baudRate)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    USART_InitStructure.USART_BaudRate = baudRate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    USART_Init(USART1, &USART_InitStructure);
    USART_Cmd(USART1, ENABLE);
    serialIsInitialized = 1;
}

PRIVATE void serialPrintByte(uint8_t ch)
{
  USART_SendData(USART1, ch);
  while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET) {}
}

PRIVATE void serialSendData(uint8_t *data, uint16_t length)
{
	if (!serialIsInitialized)
		return;

	uint16_t i;
	for (i = 0; i < length; i++)
		serialPrintByte(data[i]);
}
#else
PRIVATE void initSerialWithDma(uint32_t baudRate)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    DMA_InitTypeDef DMA_InitStruct;

    GPIO_PinAFConfig(SERIAL_PORT, SERIAL_PSRC0, SERIAL_AF);
    GPIO_PinAFConfig(SERIAL_PORT, SERIAL_PSRC1, SERIAL_AF);

    GPIO_InitStructure.GPIO_Pin = SERIAL_PINS;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(SERIAL_PORT, &GPIO_InitStructure);

    USART_InitStructure.USART_BaudRate = baudRate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(SERIAL_USART, &USART_InitStructure);

    USART_DMACmd(SERIAL_USART, USART_DMAReq_Tx, ENABLE);
    USART_DMACmd(SERIAL_USART, USART_DMAReq_Rx, ENABLE);
    USART_Cmd(SERIAL_USART, ENABLE);

    SYSCFG_DMAChannelRemapConfig(SYSCFG_DMARemap_USART1Tx, SERIAL_DMA_TX_IS_MAPPED_TO_CHANNEL4);

    // DMA - TX channel.
    DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStruct.DMA_M2M = DMA_M2M_Disable;
    DMA_InitStruct.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStruct.DMA_BufferSize = 1;
    DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)serialTxBuffer;
    DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&SERIAL_USART->TDR;
    DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStruct.DMA_Priority = DMA_Priority_Medium;
    DMA_Init(SERIAL_DMA_TX, &DMA_InitStruct);

    serialTxBuffer[0] = 0;
	DMA_Cmd(SERIAL_DMA_TX, ENABLE);

    serialTxBuffer[0] = 0;

	DMA_Cmd(SERIAL_DMA_TX, ENABLE);

	while (!DMA_GetFlagStatus(SERIAL_DMA_TX_TC)) {}
}


/**
 * Отправка данных через USART с использованием DMA.
 * Перед следующей отправкой ожидаем завершения предыдущей.
 *
 * data - буфер с данными.
 * length - длина буфера.
 */
PRIVATE void serialSendData(uint8_t *data, uint16_t length)
{
	if (length == 0)
		return;

	uint16_t i;

	// Ожидание предыдущей отправки.
	while (!DMA_GetFlagStatus(SERIAL_DMA_TX_TC)) {}
	DMA_ClearFlag(SERIAL_DMA_TX_TC);
	DMA_Cmd(SERIAL_DMA_TX, DISABLE);

	// Заполнение буфера новыми данными.
	length = MIN(length, SERIAL_TX_BUFFER_SIZE);
	for (i = 0; i < length; i++)
		serialTxBuffer[i] = data[i];

	DMA_SetCurrDataCounter(SERIAL_DMA_TX, length);
	DMA_Cmd(SERIAL_DMA_TX, ENABLE);
}

PRIVATE void serialSendDataNonBlocking(uint8_t *data, uint16_t length)
{
	if (length == 0)
		return;

	uint16_t i;

	// Ожидание предыдущей отправки.
	if (!DMA_GetFlagStatus(SERIAL_DMA_TX_TC))
		return;

	DMA_ClearFlag(SERIAL_DMA_TX_TC);
	DMA_Cmd(SERIAL_DMA_TX, DISABLE);

	// Заполнение буфера новыми данными.
	length = MIN(length, SERIAL_TX_BUFFER_SIZE);
	for (i = 0; i < length; i++)
		serialTxBuffer[i] = data[i];

	DMA_SetCurrDataCounter(SERIAL_DMA_TX, length);
	DMA_Cmd(SERIAL_DMA_TX, ENABLE);
}

PRIVATE void bufferPrintNonBlocking(void)
{
	if (printBufferLength < PRINT_BUFFER_LENGTH_MAX)
		return;
	if (!DMA_GetFlagStatus(SERIAL_DMA_TX_TC))
		return;

	serialSendDataNonBlocking(printBuffer, printBufferLength);
	printBufferLength = 0;
}
#endif

PRIVATE void bufferAdd(uint8_t *data, uint16_t length)
{
	uint16_t i;
	for (i = 0; i < length; i++)
	{
		printBuffer[printBufferLength] = data[i];
		if (printBufferLength < PRINT_BUFFER_LENGTH_MAX)
			printBufferLength++;
	}
}

PRIVATE void bufferPrint(void)
{
	serialSendData(printBuffer, printBufferLength);
	printBufferLength = 0;
}

PRIVATE void bufferPrintIfOverflow(void)
{
	if (printBufferLength < PRINT_BUFFER_LENGTH_MAX)
		return;

	serialSendData(printBuffer, printBufferLength);
	printBufferLength = 0;
}

PRIVATE void Hex24BufferPrint(ACTION_DATA(send), uint32_t *buffer, uint16_t bufSize)
{
	uint16_t i;
	for (i = 0; i < bufSize; i++)
	{
		if (i > 0)
			Common_sendStr(send, ",");
		Common_sendHex(send, buffer[i], 6);
	}
}

PRIVATE void initBuffers(void)
{
	uint16_t i;
	for (i = 0; i < PIXELS; i++)
	{
		temp_GrbBuffers[DS18B20_L][i] = 0;
		temp_GrbBuffers[DS18B20_R][i] = 0;
		ws2812in_GrbBuffers[WS2812IN_F][i] = 0;
		ws2812in_GrbBuffers[WS2812IN_B][i] = 0;
	}
}

PRIVATE void Ds18b20TempFlushTo(uint32_t (*grbBuffers)[PIXELS], Ds18b20SM_InitStruct_t *sensor)
{
    while (sensor->convertedMask > 0)
    {
    	uint8_t cursor = Ds18b20SM_InsertNextFromMask(&sensor->convertedMask);
    	uint8_t count;
    	uint32_t colorPrimary;
    	uint32_t colorSecondary;

    	int16_t temp = sensor->temp[cursor];
    	if (temp < 0)
    	{
    		colorPrimary = KSLIGHT_TEMP_COLD_COLOR_PRIMARY;
    		colorSecondary = KSLIGHT_TEMP_COLD_COLOR_SECONDARY;
    		count = -temp / 50;
    	}
    	else
    	{
    		colorPrimary = KSLIGHT_TEMP_WARM_COLOR_PRIMARY;
    		colorSecondary = KSLIGHT_TEMP_WARM_COLOR_SECONDARY;
    		count = temp / 50;
    	}

    	if (count == 0)
    		count = 1;
    	else if (count > 9)
    		count = 9;
    	count--;

    	uint8_t i;
    	for (i = 0; i < PIXELS; i++)
    	{
    		grbBuffers[cursor][REVERSE(i, PIXELS)] = i < count
				? colorSecondary
				: (i == count
					? colorPrimary
					: KSLIGHT_TEMP_COLOR_NONE);
    	}
    }
}

/////////////////////////////////////////////

void WS2812INF_TIMER_IRQ_HDL(void)
{
	Ws2812In_TimIrqHandler(&ws2812in[WS2812IN_F]);
}

void WS2812INB_TIMER_IRQ_HDL(void)
{
	Ws2812In_TimIrqHandler(&ws2812in[WS2812IN_B]);
}

void DMA1_Channel1_IRQHandler(void)
{
#if (DMA_CHANNEL_FOR_WS2812INF_N == 1)
	Ws2812In_DmaIrqHandler(&ws2812in[WS2812IN_F]);
#endif
#if (DMA_CHANNEL_FOR_WS2812INB_N == 1)
	Ws2812In_DmaIrqHandler(&ws2812in[WS2812IN_B]);
#endif
}

void DMA1_Channel2_3_IRQHandler(void)
{
#if ((DMA_CHANNEL_FOR_WS2812INF_N == 2) || (DMA_CHANNEL_FOR_WS2812INF_N == 3))
	Ws2812In_DmaIrqHandler(&ws2812in[WS2812IN_F]);
#endif
#if ((DMA_CHANNEL_FOR_WS2812INB_N == 2) || (DMA_CHANNEL_FOR_WS2812INB_N == 3))
	Ws2812In_DmaIrqHandler(&ws2812in[WS2812IN_B]);
#endif
}

void DMA1_Channel4_5_IRQHandler(void)
{
#if ((DMA_CHANNEL_FOR_WS2812INF_N == 4) || (DMA_CHANNEL_FOR_WS2812INF_N == 5))
	Ws2812In_DmaIrqHandler(&ws2812in[WS2812IN_F]);
#endif
#if ((DMA_CHANNEL_FOR_WS2812INB_N == 4) || (DMA_CHANNEL_FOR_WS2812INB_N == 5))
	Ws2812In_DmaIrqHandler(&ws2812in[WS2812IN_B]);
#endif
}

/////////////////////////////////////////////

int main(void)
{
#ifdef BOOTLOADER
	BootloaderClient_ProgramInit();
#endif

	chipReset();
	buttonInit();
	ledInit(0);

	DelayTimer_Init();
	MeasureTimer_Init(MEASURE_TIMER_US, MEASURE_PRESCALER_US);
	MeasureTimer_Init(MEASURE_TIMER_MS, MEASURE_PRESCALER_MS);

	Ws2812In_InitStructFrontFromHardwareH(&ws2812in[WS2812IN_F], ws2812In_DmaBuffers[WS2812IN_F]);
	Ws2812In_InitWithDmaAndIsr(&ws2812in[WS2812IN_F]);
	
	Ws2812In_InitStructBackFromHardwareH(&ws2812in[WS2812IN_B], ws2812In_DmaBuffers[WS2812IN_B]);
	Ws2812In_InitWithDmaAndIsr(&ws2812in[WS2812IN_B]);

	Ws2812Out_InitWithDma();

	ds18b20.enableMask = ONEWIRE_PIN_DS1820_L | ONEWIRE_PIN_DS1820_R;
	ds18b20.noPresentTicks = DS18B20_NOPRESENT_TICKS;
	ds18b20.conversionTicks = DS18B20_CONV_TICKS;
	Ds18b20SM_Init(&ds18b20);

#if (SERIAL_INITIALIZE > 0)
	initSerial(uart1BaudRate);
#endif
	Common_sendStr(serialSendData, "\r\nEUC ks-light\r\n");

	initBuffers();

	uint16_t i;
	uint16_t j;
	uint8_t circularData[PIXELS * WS2812IN_TOTAL];

	uint16_t timer1sec = 0;
	uint8_t toggle1sec = 0;
	uint8_t lastMode = 0xFF;
	uint32_t modeDuration = 0;
	while (1)
	{
		if (DelayTimer_timerMs(TICK_MS))
		{
			uint32_t ws2812out_GrbBuffers[4][PIXELS];
			for (i = 0; i < PIXELS; i++)
			{
				ws2812out_GrbBuffers[WS2812OUT_FL][i] = ws2812in_GrbBuffers[WS2812IN_F][i];
				ws2812out_GrbBuffers[WS2812OUT_FR][i] = ws2812in_GrbBuffers[WS2812IN_F][i];
				ws2812out_GrbBuffers[WS2812OUT_BL][i] = ws2812in_GrbBuffers[WS2812IN_B][i];
				ws2812out_GrbBuffers[WS2812OUT_BR][i] = ws2812in_GrbBuffers[WS2812IN_B][i];
			}

    		if (timer1sec == 0)
    		{
    			toggle1sec = 1 - toggle1sec;
    			Ws2812In_StatsPrintAndReset(serialSendData, &ws2812in[WS2812IN_F]);
    			Ws2812In_StatsPrintAndReset(serialSendData, &ws2812in[WS2812IN_B]);
    		}

			Ws2812InputsFlushTo(serialSendData, 0, ws2812in_GrbBuffers, ws2812in, ws2812In_DmaBuffers);

			if (timer1sec == 0)
			{
				Common_sendStr(serialSendData, "GRB front: ");
				Hex24BufferPrint(serialSendData, ws2812in_GrbBuffers[0], PIXELS);
				Common_sendStr(serialSendData, "\r\nGRB back: ");
				Hex24BufferPrint(serialSendData, ws2812in_GrbBuffers[1], PIXELS);
				Common_sendStr(serialSendData, "\r\n");
			}

    		Ds18b20SM_Tick();
    		Ds18b20TempFlushTo(temp_GrbBuffers, &ds18b20);

			KsLight_Mode_t lightMode;
			KsLight_GetMode(&lightMode, circularData, ws2812in_GrbBuffers);
			
			if (lastMode != 0xFF && lastMode != lightMode.mode) {
				// Lighting mode has changed.
				modeDuration = 0;
			}
			lastMode = lightMode.mode;

    		if (timer1sec == 0)
    		{
				Common_sendStr(serialSendData, "mode=");
				Common_sendDec(serialSendData, lightMode.mode, 1);
				Common_sendStr(serialSendData, " durat=");
				Common_sendDec(serialSendData, modeDuration, 5);
				Common_sendStr(serialSendData, " batt=[");
				Common_sendDec(serialSendData, lightMode.battery[0], 3);
				Common_sendStr(serialSendData, ", ");
				Common_sendDec(serialSendData, lightMode.battery[1], 3);
				Common_sendStr(serialSendData, "] green=[");
				Common_sendDec(serialSendData, lightMode.batteryGreen[0], 3);
				Common_sendStr(serialSendData, ", ");
				Common_sendDec(serialSendData, lightMode.batteryGreen[1], 3);
				Common_sendStr(serialSendData, "] temp=[");
				if (ds18b20.temp[0] >= 0)
				{
					Common_sendDec32(serialSendData, ds18b20.temp[0], 6, ' ', 1);
				}
				else
				{
					Common_sendStr(serialSendData, "-");
					Common_sendDec32(serialSendData, -ds18b20.temp[0], 5, ' ', 1);
				}
				Common_sendStr(serialSendData, ", ");
				if (ds18b20.temp[1] >= 0)
				{
					Common_sendDec32(serialSendData, ds18b20.temp[1], 6, ' ', 1);
				}
				else
				{
					Common_sendStr(serialSendData, "-");
					Common_sendDec32(serialSendData, -ds18b20.temp[1], 5, ' ', 1);
				}
				Common_sendStr(serialSendData, "] \r\n");
    		}


			switch (lightMode.mode)
			{
				case KSLIGHT_MODE_BATTERY:
				{
	    			int16_t temp[DS18B20_TOTAL];
	    			temp[DS18B20_L] = ds18b20.temp[ONEWIRE_PIN_DS1820_N_L];
	    			temp[DS18B20_R] = ds18b20.temp[ONEWIRE_PIN_DS1820_N_R];

					uint8_t isTempOK = CHECK_BIT(ds18b20.presenceMask, ONEWIRE_PIN_DS1820_N_L) && temp[DS18B20_L] != DS18B20_TEMP_ERROR;
					if (isTempOK)
					{
						if (CHECK_BIT(ds18b20.convertedMask, ONEWIRE_PIN_DS1820_N_L))
							ds18b20.convertedMask = 0;

						KsLight_TempToMode3(temp_GrbBuffers[DS18B20_L], temp[DS18B20_L], lightMode.batteryGreen[0], toggle1sec);
						for (i = 0; i < PIXELS; i++)
						{
							ws2812out_GrbBuffers[WS2812OUT_FL][i] = temp_GrbBuffers[DS18B20_L][i];
						}
					}

					isTempOK = CHECK_BIT(ds18b20.presenceMask, ONEWIRE_PIN_DS1820_N_R) && temp[DS18B20_R] != DS18B20_TEMP_ERROR;
					if (isTempOK)
					{
						if (CHECK_BIT(ds18b20.convertedMask, ONEWIRE_PIN_DS1820_N_R))
							ds18b20.convertedMask = 0;

						KsLight_TempToMode3(temp_GrbBuffers[DS18B20_R], temp[DS18B20_R], lightMode.batteryGreen[0], toggle1sec);
						for (i = 0; i < PIXELS; i++)
						{
							ws2812out_GrbBuffers[WS2812OUT_FR][i] = temp_GrbBuffers[DS18B20_R][i];
						}
					}

#if defined(KSLIGHT_BATTERYMODE_DELAY_TO_OFF)
					if (modeDuration >= KSLIGHT_BATTERYMODE_DELAY_TO_OFF)
					{
						for (i = 0; i < 4; i++)
						{

							for (j = 0; j < PIXELS; j++)
							{
							//*
								Grb_t grb;
								KsLight_GrbFromWord(&grb, ws2812out_GrbBuffers[i][j]);
								grb.g >>= 7;
								grb.r >>= 7;
								grb.b >>= 7;
								ws2812out_GrbBuffers[i][j] = KsLight_GrbToWord(&grb);
							/*/
								ws2812out_GrbBuffers[i][j] = 0;
							// */
							}
						}
					}
#endif
					break;
				}

				/*case KSLIGHT_MODE_RIDE:
				{
					uint8_t maxPoint = 0;
					uint8_t minPoint = 0;
					for (i = 1; i < PIXELS * WS2812IN_TOTAL; i++)
					{
						if (circularData[i] > circularData[maxPoint])
							maxPoint = i;
						else if (circularData[i] > 0 &&
								(circularData[minPoint] == 0 || circularData[i] < circularData[minPoint]))
							minPoint = i;
					}

					int8_t dir = (minPoint < maxPoint)
						? ((maxPoint - minPoint) < PIXELS ? -1 : 1)
						: ((minPoint - maxPoint) < PIXELS ? 1 : -1);

					uint32_t circular_GrbBuffers[WS2812IN_TOTAL * PIXELS];
					for (i = 0; i < WS2812IN_TOTAL * PIXELS; i++)
						circular_GrbBuffers[i] = 0;

					uint8_t maxPointOpposite;
					CIRCLE_ADD_TO(maxPointOpposite, maxPoint, PIXELS, WS2812IN_TOTAL * PIXELS);
					circular_GrbBuffers[maxPoint] = 0x001F00;
					circular_GrbBuffers[maxPointOpposite] = 0x1F0000;
					if (dir > 0)
					{
						CIRCLE_INCREMENT(maxPoint, WS2812IN_TOTAL * PIXELS);
						CIRCLE_INCREMENT(maxPointOpposite, WS2812IN_TOTAL * PIXELS);
					}
					else
					{
						CIRCLE_DECREMENT(maxPoint, WS2812IN_TOTAL * PIXELS);
						CIRCLE_DECREMENT(maxPointOpposite, WS2812IN_TOTAL * PIXELS);
					}
					circular_GrbBuffers[maxPoint] = 0x000700;
					circular_GrbBuffers[maxPointOpposite] = 0x070000;

					uint32_t circular_GrbBufferReversed[PIXELS];
					for (i = 0; i < PIXELS; i++)
						circular_GrbBufferReversed[i] = circular_GrbBuffers[WS2812IN_B * PIXELS + REVERSE(i, PIXELS)];

					fromBuffers[WS2812OUT_FL] = circular_GrbBuffers;
					fromBuffers[WS2812OUT_FR] = circular_GrbBuffers;
					fromBuffers[WS2812OUT_BL] = circular_GrbBufferReversed;
					fromBuffers[WS2812OUT_BR] = circular_GrbBufferReversed;
					break;
				}*/
				
				default:
					break;
			}

			for (i = 0; i < WS2812OUT_TOTAL; i++)
				Ws2812Out_FromBuffer(ws2812Out_DmaBuffers[i], ws2812out_GrbBuffers[i], PIXELS);

			Ws2812Out_DmaSend(ws2812Out_DmaBuffers);
			CIRCLE_INCREMENT(timer1sec, TIMER1SEC_TICKS);
			modeDuration++;
		}
	}
}
