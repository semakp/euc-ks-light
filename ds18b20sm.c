﻿#include "ds18b20sm.h"
#include "common.h"
#include "stm32f0xx.h"

//ONEWIRE
//commands
#define OWCMD_SKIP_ROM 			0xCC
#define OWCMD_READ_ROM 			0x33
#define OWCMD_MATCH_ROM 		0x55
#define OWCMD_SEARCH_ROM 		0xF0
#define OWCMD_ALARM_SEARCH 		0xEC
//ds18b20
#define OWCMD_CONV_TEMP 		0x44
#define OWCMD_READ_SCTPAD 		0xBE
#define OWCMD_COPY_SCTPAD 		0x48
#define OWCMD_WRITE_SCTPAD 		0x4E

#define DS18B20STATE_RESET				0
#define DS18B20STATE_WAIT_FOR_RESET		1
#define DS18B20STATE_BEFORE_CONV_TEMP	2
#define DS18B20STATE_CONV_TEMP			3
#define DS18B20STATE_WAIT_CONVERSION	4
#define DS18B20STATE_RESET_AFTER_CONV	5
#define DS18B20STATE_BEFORE_READ_SCTPAD	6
#define DS18B20STATE_READ_SCTPAD		7
#define DS18B20STATE_READING_TEMP		8

#define __START_CRITICAL_SECTION__	{__disable_irq();}
#define __END_CRITICAL_SECTION__	{__enable_irq();}

#define DS18B20SM_CURSOR_MAXIMUM	16

PRIVATE Ds18b20SM_InitStruct_t *ds18b20sm;
PRIVATE uint8_t state;
PRIVATE uint8_t cursor;
PRIVATE uint16_t readingMask;
PRIVATE uint16_t loopTick;
PRIVATE uint8_t scratchPad[9];

PRIVATE int16_t GetTempFromArray(uint8_t *tempArray);
PRIVATE uint16_t OneWireReset(uint16_t mask);
PRIVATE void OneWireWriteByte(uint16_t mask, uint8_t data);
PRIVATE uint8_t OneWireReadByte(uint16_t mask);
PRIVATE uint8_t Crc8(uint8_t *pcBlock, uint16_t len);

PRIVATE inline void Delay_500us(void)
{
 	delay_us(500);
}
PRIVATE inline void Delay_70us(void)
{
 	delay_us(70);
}
PRIVATE inline void Delay_7us(void)
{
 	delay_us(4);
}

PRIVATE inline void OW_Bus_Clear(uint16_t mask)
{
	ONEWIRE_PORT->BSRR = mask;
}

PRIVATE inline void OW_Bus_Set(uint16_t mask)
{
	ONEWIRE_PORT->BRR = mask;
}

PRIVATE inline uint16_t OW_Bus_Get(uint16_t mask)
{
	return (ONEWIRE_PORT->IDR & mask);
}

PUBLIC void Ds18b20SM_Init(Ds18b20SM_InitStruct_t *initStruct)
{
	state = DS18B20STATE_RESET;
	loopTick = 0;
	ds18b20sm = initStruct;

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = ds18b20sm->enableMask;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(ONEWIRE_PORT, &GPIO_InitStructure);
	GPIO_SetBits(ONEWIRE_PORT, ds18b20sm->enableMask);

	uint8_t i;
	for (i = 0; i < 16; i++)
		ds18b20sm->temp[i] = DS18B20_TEMP_ERROR;
}

PUBLIC void Ds18b20SM_Tick(void)
{
	switch (state)
	{
		case DS18B20STATE_RESET:
			ds18b20sm->presenceMask = OneWireReset(ds18b20sm->enableMask);
			state = ds18b20sm->presenceMask > 0
				? DS18B20STATE_BEFORE_CONV_TEMP 
				: DS18B20STATE_WAIT_FOR_RESET;
			break;

		case DS18B20STATE_WAIT_FOR_RESET:
 			loopTick++;
			if (loopTick >= ds18b20sm->noPresentTicks)
			{
				state = DS18B20STATE_RESET;
				loopTick = 0;
			}
			break;

		case DS18B20STATE_BEFORE_CONV_TEMP:
 			OneWireWriteByte(ds18b20sm->presenceMask, OWCMD_SKIP_ROM);
			state = DS18B20STATE_CONV_TEMP;
			break;

		case DS18B20STATE_CONV_TEMP:
 			OneWireWriteByte(ds18b20sm->presenceMask, OWCMD_CONV_TEMP);
			state = DS18B20STATE_WAIT_CONVERSION;
			break;

		case DS18B20STATE_WAIT_CONVERSION:
 			loopTick++;
			if (loopTick >= ds18b20sm->conversionTicks)
			{
				state = DS18B20STATE_RESET_AFTER_CONV;
				loopTick = 0;
			}
			break;

		case DS18B20STATE_RESET_AFTER_CONV:
			ds18b20sm->presenceMask = OneWireReset(ds18b20sm->enableMask);
			state = ds18b20sm->presenceMask > 0
				? DS18B20STATE_BEFORE_READ_SCTPAD 
				: DS18B20STATE_WAIT_FOR_RESET;
			break;

		case DS18B20STATE_BEFORE_READ_SCTPAD:
 			OneWireWriteByte(ds18b20sm->presenceMask, OWCMD_SKIP_ROM);
			state = DS18B20STATE_READ_SCTPAD;
			break;

		case DS18B20STATE_READ_SCTPAD:
 			OneWireWriteByte(ds18b20sm->presenceMask, OWCMD_READ_SCTPAD);
			state = DS18B20STATE_READING_TEMP;
			readingMask = ds18b20sm->presenceMask;
			cursor = Ds18b20SM_InsertNextFromMask(&readingMask);
			break;

		case DS18B20STATE_READING_TEMP:
			scratchPad[loopTick] = OneWireReadByte(1 << cursor);
 			loopTick++;
			if (loopTick >= 9)
			{
				ds18b20sm->temp[cursor] = GetTempFromArray(scratchPad);
				SETBIT(ds18b20sm->convertedMask, cursor);
				cursor = Ds18b20SM_InsertNextFromMask(&readingMask);
				if (cursor == DS18B20SM_CURSOR_MAXIMUM)
					state = DS18B20STATE_RESET;
				loopTick = 0;
			}
			break;

	}
}

PUBLIC uint8_t Ds18b20SM_InsertNextFromMask(uint16_t *mask)
{
	if (*mask == 0)
		return DS18B20SM_CURSOR_MAXIMUM;

	uint8_t i;
	for (i = 0; i < DS18B20SM_CURSOR_MAXIMUM; i++)
		if (CHECK_BIT(*mask, i))
			break;

	CLRBIT(*mask, i);
	return i;
}





PRIVATE int16_t GetTempFromArray(uint8_t *tempArray)
{
  	if(Crc8(tempArray, 8) != tempArray[8])
  		return DS18B20_TEMP_ERROR;

  	uint8_t lsb = tempArray[0];
  	uint8_t msb = tempArray[1];
  	uint8_t negative = 0;
  	int16_t cur = ((int)msb << 8) + lsb;
  	if (cur < 0)
  	{
  	   negative = 1;
  	   cur = -cur;
  	}

  	cur = (cur * 10) / 16;
  	if (negative == 1) 
		cur = -cur;

  	return cur;
}

PRIVATE uint16_t OneWireReset(uint16_t mask)
{
 	OW_Bus_Clear(mask);
 	mask &= OW_Bus_Get(mask);
 	if (mask == 0) // detect 0V on bus error
 		return 0;

	__START_CRITICAL_SECTION__
 	OW_Bus_Set(mask);
 	Delay_500us();
 	OW_Bus_Clear(mask);
 	Delay_70us();
 	uint16_t result = OW_Bus_Get(mask);
	__END_CRITICAL_SECTION__
	//Delay_500us();
 	return (~result) & mask;
}

PRIVATE void OneWireWriteByte(uint16_t mask, uint8_t data)
{
	uint8_t i;
 	for (i = 0; i <= 7; i++)
 	{
	    __START_CRITICAL_SECTION__
 		OW_Bus_Set(mask); 			//ONEDDR |= mask;
 	    if (data & 0x01)
 		{
 			Delay_7us();           	// Send 1
 			OW_Bus_Clear(mask); 	//ONEDDR &= ~(mask);
 	        __END_CRITICAL_SECTION__
 	        Delay_70us();
 	    }
 	    else
 	    {
 	        Delay_70us();         	// Send 0
 	        OW_Bus_Clear(mask); 	//ONEDDR &= ~(mask);
 	        __END_CRITICAL_SECTION__
 	        Delay_7us();
 	    }
 	    data >>= 1;
     }
}

PRIVATE uint8_t OneWireReadByte(uint16_t mask)
{
 	unsigned char data = 0;
 	unsigned char i;
 	for (i = 0; i <= 7; i++)
 	{
	    __START_CRITICAL_SECTION__
 		OW_Bus_Set(mask);
 		Delay_7us();
 		OW_Bus_Clear(mask);
 		Delay_7us();
 		data >>= 1;
 		if (OW_Bus_Get(mask))
 			data |= 0x80;
 		else data &= 0x7f;
	    __END_CRITICAL_SECTION__
 		Delay_70us();
 	}
 	return data;
}

PRIVATE uint8_t Crc8(uint8_t *pcBlock, uint16_t len)
{
	uint8_t crc = 0x00;
	uint8_t data;
    uint16_t i;

    while (len--)
    {
        data = *pcBlock++;
        for (i = 0; i < 8; i++) {
            crc = (crc ^ data) & 0x01
            	? ((crc ^ 0x18) >> 1) | 0x80
            	: crc >> 1;
            data >>= 1;
        }
    }

    return crc;
}
