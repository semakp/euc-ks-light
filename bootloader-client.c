﻿#include "bootloader-client.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_syscfg.h"
#include "stm32f0xx_flash.h"

PUBLIC void BootloaderClient_ProgramInit(void)
{
	if (BOOTLOADER_CONFIG_POK_VALUE == 0xFFFFFFFFUL)
	{
		FLASH_Unlock();
		FLASH_ProgramWord(BOOTLOADER_CONFIG_POK_ADDRESS, BOOTLOADER_PROGRAM_IS_OK);
		FLASH_Lock();
	}

    volatile uint32_t *VectorTable = (volatile uint32_t *)0x20000000;
    uint32_t ui32_VectorIndex = 0;
    for (ui32_VectorIndex = 0; ui32_VectorIndex < 48; ui32_VectorIndex++) {
        VectorTable[ui32_VectorIndex] = *(volatile uint32_t*)((uint32_t)HW_FLASH_PSTART_ADDRESS + (ui32_VectorIndex << 2));
    }

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    SYSCFG_MemoryRemapConfig(SYSCFG_MemoryRemap_SRAM);
    __enable_irq();
}

PUBLIC void BootloaderClient_StartBootloader(uint32_t key)
{
	FLASH_Unlock();
	if (BOOTLOADER_CONFIG_BTLKEY_VALUE != 0xFFFFFFFFUL)
		FLASH_ErasePage(BOOTLOADER_CONFIG_PAGE_ADDRESS);

	FLASH_ProgramWord(BOOTLOADER_CONFIG_BTLKEY_ADDRESS, key);
	FLASH_Lock();
}
