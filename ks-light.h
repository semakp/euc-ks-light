﻿#ifndef __KS_LIGHT__
#define __KS_LIGHT__

#include "stdafx.h"
#include "stm32f0xx.h"
#include "hardware.h"

#define REVERSE(inx, size) 		(((size) - 1) - (inx))

#define KSLIGHT_MODE_OFF		0
#define KSLIGHT_MODE_BATTERY	1
#define KSLIGHT_MODE_RIDE		2
#define KSLIGHT_MODE_UNKNOWN	3

#define KSLIGHT_BATTERY_COLOR_PRIMARY		0x070000
#define KSLIGHT_BATTERY_COLOR_SECONDARY		0x010000
#define KSLIGHT_BATTERY_COLOR_NONE			0x000000

#define KSLIGHT_TEMP_WARM_COLOR_PRIMARY		0x000700
#define KSLIGHT_TEMP_WARM_COLOR_SECONDARY	0x000100
#define KSLIGHT_TEMP_COLD_COLOR_PRIMARY		0x000007
#define KSLIGHT_TEMP_COLD_COLOR_SECONDARY	0x000001
#define KSLIGHT_TEMP_COLOR_NONE				0x000000


#define MODE2_GROUP_COUNT			4
#define MODE2_TEMP_GROUP_STEP		(200) // группы по 20 градусов

#define MODE2_PIXEL_STEP_1K	(((uint32_t)(256) * 1000) / (PIXELS))
#define MODE2_PIXEL_STEP 		((256) / (PIXELS))

#define MODE2_INTENSE_MAXIMUM		(255)

// MODE2_COLOR_FROZEN	ниже -20 - мигает 9 фиолетовых
// MODE2_COLOR_COLD от -20 до 0 - сине-зелёный от 1 до 9		-20.0 -> 0-199
// MODE2_COLOR_COOL	от 0 до 20 - жёлто-зелёный от 1 до 9		0 -> 200-399
// MODE2_COLOR_WARM	от 20 до 40 - жёлто-оранжевый от 1 до 9		20.0 -> 400-599
// MODE2_COLOR_HOT	 от 40 до 60 - красный от 1 до 9			40.0 -> 600-799
// MODE2_COLOR_HOT	больше 60 - мигание красного				60.0 -> 800+
#define MODE2_COLOR_COLD			0
#define MODE2_COLOR_COOL			1
#define MODE2_COLOR_WARM			2
#define MODE2_COLOR_HOT				3
#define MODE2_COLOR_FROZEN			4 // - специальный!


#define MODE3_PIXEL_STEP_1K	(((uint32_t)(256) * 1000) / (PIXELS))
#define MODE3_PIXEL_STEP 		((256) / (PIXELS))

#define MODE3_INTENSE_MAXIMUM		(255)
#define MODE3_COLOR_COOL			0
#define MODE3_COLOR_HOT				1
#define MODE3_COLOR_FROZEN			2 // - специальный!



typedef struct
{
	uint8_t mode;
	uint8_t battery[2];
	uint8_t batteryGreen[2];
}
KsLight_Mode_t;

typedef struct
{
	uint8_t g;
	uint8_t r;
	uint8_t b;
}
Grb_t;

PUBLIC void KsLight_GrbFromWord(Grb_t *grb, uint32_t pixel);

PUBLIC uint32_t KsLight_GrbToWord(Grb_t *grb);

// less than -20
// violet
PUBLIC void KsLight_GetGrbFrozen(Grb_t *grb, uint8_t brightness, uint8_t intense);

// from -20 to 0
// purpre to blue
PUBLIC void KsLight_GetGrbCold(Grb_t *grb, uint8_t brightness, uint8_t intense);

// from 0 to 20
// cyan to spring-green
PUBLIC void KsLight_GetGrbCool(Grb_t *grb, uint8_t brightness, uint8_t intense);

//from 20 to 40
//acidgreen - yellow
PUBLIC void KsLight_GetGrbWarm(Grb_t *grb, uint8_t brightness, uint8_t intense);

//from 40 to 60
//orange - red
PUBLIC void KsLight_GetGrbHot(Grb_t *grb, uint8_t brightness, uint8_t intense);

PUBLIC void KsLight_GetMode(KsLight_Mode_t *out, uint8_t *circularData, uint32_t (*grbBuffers)[PIXELS]);

PUBLIC void KsLight_TempToMode2(uint32_t *grbBuffer, int16_t temp, uint8_t brightness, uint8_t toggle);

PUBLIC void KsLight_TempToMode3(uint32_t *grbBuffer, int16_t temp, uint8_t brightness, uint8_t toggle);

#endif /* __KS_LIGHT__ */
