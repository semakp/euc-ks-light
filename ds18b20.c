﻿#include "ds18b20.h"
#include "common.h"
#include "stm32f0xx.h"

//ONEWIRE
//commands
#define OWCMD_SKIP_ROM 			0xCC
#define OWCMD_READ_ROM 			0x33
#define OWCMD_MATCH_ROM 		0x55
#define OWCMD_SEARCH_ROM 		0xF0
#define OWCMD_ALARM_SEARCH 		0xEC
//ds18b20
#define OWCMD_CONV_TEMP 		0x44
#define OWCMD_READ_SCTPAD 		0xBE
#define OWCMD_COPY_SCTPAD 		0x48
#define OWCMD_WRITE_SCTPAD 		0x4E

#define __START_CRITICAL_SECTION__	{__disable_irq();}
#define __END_CRITICAL_SECTION__	{__enable_irq();}

PRIVATE uint16_t OneWireReset(uint16_t mask);
PRIVATE void OneWireWriteByte(uint16_t mask, uint8_t data);
PRIVATE uint8_t OneWireReadByte(uint16_t mask);
PRIVATE uint8_t Crc8(uint8_t *pcBlock, uint16_t len);

PRIVATE inline void Delay_500us(void)
{
 	delay_us(500);
}
PRIVATE inline void Delay_70us(void)
{
 	delay_us(70);
}
PRIVATE inline void Delay_7us(void)
{
 	delay_us(4);
}

PRIVATE inline void OW_Bus_Clear(uint16_t mask)
{
	ONEWIRE_PORT->BSRR = mask;
}

PRIVATE inline void OW_Bus_Set(uint16_t mask)
{
	ONEWIRE_PORT->BRR = mask;
}

PRIVATE inline uint16_t OW_Bus_Get(uint16_t mask)
{
	return (ONEWIRE_PORT->IDR & mask);
}

PUBLIC void Ds18b20_Init(uint16_t mask)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = mask;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(ONEWIRE_PORT, &GPIO_InitStructure);
	GPIO_SetBits(ONEWIRE_PORT, mask);
}

PUBLIC uint8_t Ds18b20_ConvertTemp(uint16_t mask)
{
	uint16_t present = OneWireReset(mask);
 	if (present == 0)
 		return 0;

 	OneWireWriteByte(present, OWCMD_SKIP_ROM);
 	OneWireWriteByte(present, OWCMD_CONV_TEMP);
 	return present;
}

PUBLIC int16_t Ds18b20_GetTemp(uint16_t mask)
{
 	uint8_t msb = 0, lsb = 0;
 	uint8_t negative;
  	int16_t cur;
  	uint8_t t[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
  	uint8_t i;

  	if (!OneWireReset(mask))
  		return 9999;

  	OneWireWriteByte(mask, OWCMD_SKIP_ROM);
  	OneWireWriteByte(mask, OWCMD_READ_SCTPAD);

  	for(i = 0; i < 9; i++)
  	{
  		t[i] = OneWireReadByte(mask);
  	}
  	OneWireReset(mask);

  	if(Crc8(t, 8) != t[8])
  		return 9999;

  	lsb = t[0];
  	msb = t[1];

  	negative = 0;
  	cur = ((int)msb << 8) + lsb;
  	if (cur < 0)
  	{
  	   negative = 1;
  	   cur = -cur;
  	}
  	cur = (cur * 10) / 16;
  	if (negative == 1) cur = -cur;

  	return cur;
}

PRIVATE uint16_t OneWireReset(uint16_t mask)
{
 	OW_Bus_Clear(mask);
 	mask &= OW_Bus_Get(mask);
 	if (mask == 0) // detect 0V on bus error
 		return 0;

	__START_CRITICAL_SECTION__
 	OW_Bus_Set(mask);
 	Delay_500us();
 	OW_Bus_Clear(mask);
 	Delay_70us();
 	uint16_t result = OW_Bus_Get(mask);
	__END_CRITICAL_SECTION__
	Delay_500us();
 	return (~result) & mask;
}

PRIVATE void OneWireWriteByte(uint16_t mask, uint8_t data)
{
	uint8_t i;
 	for (i = 0; i <= 7; i++)
 	{
	    __START_CRITICAL_SECTION__
 		OW_Bus_Set(mask); 			//ONEDDR |= mask;
 	    if (data & 0x01)
 		{
 			Delay_7us();           	// Send 1
 			OW_Bus_Clear(mask); 	//ONEDDR &= ~(mask);
 	        __END_CRITICAL_SECTION__
 	        Delay_70us();
 	    }
 	    else
 	    {
 	        Delay_70us();         	// Send 0
 	        OW_Bus_Clear(mask); 	//ONEDDR &= ~(mask);
 	        __END_CRITICAL_SECTION__
 	        Delay_7us();
 	    }
 	    data >>= 1;
     }
}

PRIVATE uint8_t OneWireReadByte(uint16_t mask)
{
 	unsigned char data = 0;
 	unsigned char i;
 	for (i = 0; i <= 7; i++)
 	{
	    __START_CRITICAL_SECTION__
 		OW_Bus_Set(mask);
 		Delay_7us();
 		OW_Bus_Clear(mask);
 		Delay_7us();
 		data >>= 1;
 		if (OW_Bus_Get(mask))
 			data |= 0x80;
 		else data &= 0x7f;
	    __END_CRITICAL_SECTION__
 		Delay_70us();
 	}
 	return data;
}

PRIVATE uint8_t Crc8(uint8_t *pcBlock, uint16_t len)
{
	uint8_t crc = 0x00;
	uint8_t data;
    uint16_t i;

    while (len--)
    {
        data = *pcBlock++;
        for (i = 0; i < 8; i++) {
            crc = (crc ^ data) & 0x01
            	? ((crc ^ 0x18) >> 1) | 0x80
            	: crc >> 1;
            data >>= 1;
        }
    }

    return crc;
}