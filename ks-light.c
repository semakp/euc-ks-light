﻿#include "ks-light.h"
#include "common.h"

PRIVATE inline uint8_t GrbIsBatteryGreen(Grb_t *grb)
{
	return ((grb->g >> 1) > grb->r)
		&& ((grb->g >> 1) > grb->b);
}

PRIVATE inline uint8_t GrbIsPixelOff(Grb_t *grb)
{
	return( grb->g < 0x07)
		&& (grb->r < 0x07)
		&& (grb->b < 0x07);
}

PRIVATE inline uint8_t GrbGetCircularOrange(Grb_t *grb)
{
	return ((grb->r > grb->g)
		&& (grb->r > 0x07)
		&& (grb->b < 0x07))
			? grb->r
			: 0;
}


PUBLIC inline void KsLight_GrbFromWord(Grb_t *grb, uint32_t pixel)
{
	grb->g = (pixel >> 16) & 0xFF;
	grb->r = (pixel >> 8) & 0xFF;
	grb->b = pixel & 0xFF;
}

PUBLIC inline uint32_t KsLight_GrbToWord(Grb_t *grb)
{
	return ((uint32_t)grb->g << 16) | ((uint32_t)grb->r << 8) | grb->b;
}

// less than -20
// violet
PUBLIC inline void KsLight_GetGrbFrozen(Grb_t *grb, uint8_t brightness, uint8_t intense)
{
	grb->r = brightness >> 1;
	grb->g = 0;
	grb->b = brightness;
}

// from -20 to 0
// purpre to blue
PUBLIC inline void KsLight_GetGrbCold(Grb_t *grb, uint8_t brightness, uint8_t intense)
{
	grb->r = ((uint32_t)brightness * (255 - intense)) / 512;//from half to 0:
	grb->g = 0;
	grb->b = brightness;
}

// from 0 to 20
// cyan to spring-green
PUBLIC inline void KsLight_GetGrbCool(Grb_t *grb, uint8_t brightness, uint8_t intense)
{
	grb->r = 0;
	grb->g = brightness;
	grb->b = ((uint32_t)brightness * (511 - intense)) / 511; //full to half
}

//from 20 to 40
//acidgreen - yellow
PUBLIC inline void KsLight_GetGrbWarm(Grb_t *grb, uint8_t brightness, uint8_t intense)
{
	grb->r = ((uint32_t)brightness * (intense + 256)) / 511; // from half to full
	grb->g = brightness;
	grb->b = 0;
}

//from 40 to 60
//orange - red
PUBLIC inline void KsLight_GetGrbHot(Grb_t *grb, uint8_t brightness, uint8_t intense)
{
	grb->r = brightness;
	grb->g = ((uint32_t)brightness * (255 - intense)) / 512;//from half to 0:
	grb->b = 0;
}

PUBLIC void KsLight_GetMode(KsLight_Mode_t *out, uint8_t *circularData, uint32_t (*grbBuffers)[PIXELS])
{
	out->battery[WS2812IN_F] = 0;
	out->battery[WS2812IN_B] = 0;
	out->batteryGreen[WS2812IN_F] = 0x7F;
	out->batteryGreen[WS2812IN_B] = 0x7F;

	uint8_t isModeOff = 1;
	uint8_t isMaybeCurcular = 1;

	uint8_t i;
	for (i = 0; i < PIXELS; i++)
	{
		Grb_t pixel[WS2812IN_TOTAL];
		KsLight_GrbFromWord(&pixel[WS2812IN_F], grbBuffers[WS2812IN_F][REVERSE(i, PIXELS)]);
		KsLight_GrbFromWord(&pixel[WS2812IN_B], grbBuffers[WS2812IN_B][(PIXELS - 1) - i]);

		uint8_t isOff[WS2812IN_TOTAL];
		isOff[WS2812IN_F] = GrbIsPixelOff(&pixel[WS2812IN_F]);
		isOff[WS2812IN_B] = GrbIsPixelOff(&pixel[WS2812IN_B]);

		if (!isOff[WS2812IN_F] || !isOff[WS2812IN_B])
			isModeOff = 0;

		if (out->battery[WS2812IN_F] > 0 || i == 0)
		{
			if (GrbIsBatteryGreen(&pixel[WS2812IN_F]))
			{
				out->battery[WS2812IN_F]++;
				out->batteryGreen[WS2812IN_F] = pixel[WS2812IN_F].g;
			}
			else if (!isOff[WS2812IN_F])
				out->battery[WS2812IN_F] = 0;
		}

		if (out->battery[WS2812IN_B] > 0 || i == 0)
		{
			if (GrbIsBatteryGreen(&pixel[WS2812IN_B]))
			{
				out->battery[WS2812IN_B]++;
				out->batteryGreen[WS2812IN_B] = pixel[WS2812IN_B].g;
			}
			else if (!isOff[WS2812IN_B])
				out->battery[WS2812IN_B] = 0;
		}

		if (isMaybeCurcular > 0)
		{
			uint8_t circOrange[WS2812IN_TOTAL];
			circOrange[WS2812IN_F] = GrbGetCircularOrange(&pixel[WS2812IN_F]);
			circOrange[WS2812IN_B] = GrbGetCircularOrange(&pixel[WS2812IN_B]);

			if ((!isOff[WS2812IN_F] && circOrange[WS2812IN_F] == 0)
				|| (!isOff[WS2812IN_B] && circOrange[WS2812IN_B] == 0))
			{
				isMaybeCurcular = 0;
			}
			else
			{
				circularData[WS2812IN_F * PIXELS + i] = circOrange[WS2812IN_F];
				circularData[WS2812IN_B * PIXELS + (PIXELS - 1) - i] = circOrange[WS2812IN_B];
			}
		}
	}

	if (isModeOff)
		out->mode = KSLIGHT_MODE_OFF;
	else if ((out->battery[WS2812IN_F] > 0) && (out->battery[WS2812IN_B] > 0))
		out->mode = KSLIGHT_MODE_BATTERY;
	else if (isMaybeCurcular)
		out->mode = KSLIGHT_MODE_RIDE;
	else
		out->mode = KSLIGHT_MODE_UNKNOWN;
}

PUBLIC void KsLight_TempToMode2(uint32_t *grbBuffer, int16_t temp, uint8_t brightness, uint8_t toggle)
{
	uint8_t intenseOfAllNormalized;
	uint8_t colorNumber;

	if (temp < -MODE2_TEMP_GROUP_STEP) 
	{
		intenseOfAllNormalized = MODE2_INTENSE_MAXIMUM;
		colorNumber = MODE2_COLOR_FROZEN;
		if (toggle)
		{
			brightness = 0;
		}
	}
	else if (temp >= MODE2_TEMP_GROUP_STEP * (MODE2_GROUP_COUNT - 1)) 
	{
		intenseOfAllNormalized = MODE2_INTENSE_MAXIMUM;
		colorNumber = MODE2_COLOR_HOT;
		if (toggle)
		{
			brightness = 0;
		}
	}
	else
	{
		// Нормализованная температура (0-799).
		uint16_t tempNormalize = temp + MODE2_TEMP_GROUP_STEP;
		// Номер цвета (0-3).
		colorNumber = tempNormalize / MODE2_TEMP_GROUP_STEP;
		// Интенсивность (0-199).
		uint8_t intenseOfAll = tempNormalize - colorNumber * MODE2_TEMP_GROUP_STEP;
		// Интенсивность нормализованная (0-255).
		intenseOfAllNormalized = ((uint16_t)intenseOfAll * 256) / MODE2_TEMP_GROUP_STEP;
	}

	{
		uint32_t intenseOfAllNormalized1k = (uint32_t)intenseOfAllNormalized * 1000;
		uint8_t pixelsCount = intenseOfAllNormalized1k / MODE2_PIXEL_STEP_1K;
		if (pixelsCount >= PIXELS)
		{
			pixelsCount = PIXELS - 1;
		}

		// Интенсивность последнего пикселя (0-27).
		uint8_t intenseOfLast = (intenseOfAllNormalized1k - MODE2_PIXEL_STEP_1K * pixelsCount) / 1000;
		if (intenseOfLast >= MODE2_PIXEL_STEP)
		{
			intenseOfLast = MODE2_PIXEL_STEP - 1;
		}

		// Интенсивность последнего пикселя после нормализации (1-16).
		uint8_t intenseOfLastNormalized = ((uint16_t)intenseOfLast * 16) / MODE2_PIXEL_STEP + 1;

		// Яркость последнего пикселя.
		uint8_t brightnessOfLast = (((uint16_t)brightness * intenseOfLastNormalized) / 16);

		Grb_t grb;
		Grb_t grbLast;
		if (colorNumber == MODE2_COLOR_FROZEN) 
		{
			KsLight_GetGrbFrozen(&grb, brightness, intenseOfAllNormalized);
			KsLight_GetGrbFrozen(&grbLast, brightnessOfLast, intenseOfAllNormalized);
		}
		else if (colorNumber == MODE2_COLOR_COLD)
		{
			KsLight_GetGrbCold(&grb, brightness, intenseOfAllNormalized);
			KsLight_GetGrbCold(&grbLast, brightnessOfLast, intenseOfAllNormalized);
		}
		else if (colorNumber == MODE2_COLOR_COOL)
		{
			KsLight_GetGrbCool(&grb, brightness, intenseOfAllNormalized);
			KsLight_GetGrbCool(&grbLast, brightnessOfLast, intenseOfAllNormalized);
		}
		else if (colorNumber == MODE2_COLOR_WARM)
		{
			KsLight_GetGrbWarm(&grb, brightness, intenseOfAllNormalized);
			KsLight_GetGrbWarm(&grbLast, brightnessOfLast, intenseOfAllNormalized);
		}
		else /*if (colorNumber == MODE2_COLOR_HOT)*/
		{
			KsLight_GetGrbHot(&grb, brightness, intenseOfAllNormalized);
			KsLight_GetGrbHot(&grbLast, brightnessOfLast, intenseOfAllNormalized);
		}

		uint32_t color = KsLight_GrbToWord(&grb);
		uint32_t colorOfLast = KsLight_GrbToWord(&grbLast);

		uint8_t i;
		for (i = 0; i < PIXELS; i++)
		{
			grbBuffer[REVERSE(i, PIXELS)] = i < pixelsCount
				? color
				: (i == pixelsCount
					? colorOfLast
					: KSLIGHT_TEMP_COLOR_NONE);
		}
	}
}

PUBLIC void KsLight_TempToMode3(uint32_t *grbBuffer, int16_t temp, uint8_t brightness, uint8_t toggle)
{
	uint8_t intenseOfAllNormalized;
	uint8_t colorNumber;

	if (temp < -200)
	{
		intenseOfAllNormalized = MODE3_INTENSE_MAXIMUM;
		colorNumber = MODE3_COLOR_FROZEN;
		if (toggle)
		{
			brightness = 0;
		}
	}
	else if (temp >= 600)
	{
		intenseOfAllNormalized = MODE3_INTENSE_MAXIMUM;
		colorNumber = MODE3_COLOR_HOT;
		if (toggle)
		{
			brightness = 0;
		}
	} else {
		// Нормализованная температура (0-799).
		uint16_t tempNormalize = temp + 200;
		// Номер цвета.
		colorNumber = MODE3_COLOR_COOL;
		// Интенсивность (0-199).
		uint8_t intenseOfAll = tempNormalize / 4;
		// Интенсивность нормализованная (0-255).
		intenseOfAllNormalized = ((uint16_t)intenseOfAll * 256) / MODE2_TEMP_GROUP_STEP;
	}

	{
		uint32_t intenseOfAllNormalized1k = (uint32_t)intenseOfAllNormalized * 1000;
		uint8_t pixelsCount = intenseOfAllNormalized1k / MODE3_PIXEL_STEP_1K;
		if (pixelsCount >= PIXELS)
			pixelsCount = PIXELS - 1;

		// Интенсивность последнего пикселя (0-27).
		uint8_t intenseOfLast = (intenseOfAllNormalized1k - MODE3_PIXEL_STEP_1K * pixelsCount) / 1000;
		if (intenseOfLast >= MODE3_PIXEL_STEP)
			intenseOfLast = MODE3_PIXEL_STEP - 1;

		// Интенсивность последнего пикселя после нормализации (1-16).
		uint8_t intenseOfLastNormalized = ((uint16_t)intenseOfLast * 16) / MODE3_PIXEL_STEP + 1;

		// Яркость последнего пикселя.
		uint8_t brightnessOfLast = (((uint16_t)brightness * intenseOfLastNormalized) / 16);

		Grb_t grb;
		Grb_t grbLast;
		if (colorNumber == MODE3_COLOR_FROZEN)
		{
			KsLight_GetGrbFrozen(&grb, brightness, intenseOfAllNormalized);
			KsLight_GetGrbFrozen(&grbLast, brightnessOfLast, intenseOfAllNormalized);
		}
		else if (colorNumber == MODE3_COLOR_COOL)
		{
			KsLight_GetGrbCool(&grb, brightness, intenseOfAllNormalized);
			KsLight_GetGrbCool(&grbLast, brightnessOfLast, intenseOfAllNormalized);
		}
		else /*if (colorNumber == MODE3_COLOR_HOT)*/
		{
			KsLight_GetGrbHot(&grb, brightness, intenseOfAllNormalized);
			KsLight_GetGrbHot(&grbLast, brightnessOfLast, intenseOfAllNormalized);
		}

		uint32_t color = KsLight_GrbToWord(&grb);
		uint32_t colorOfLast = KsLight_GrbToWord(&grbLast);

		uint8_t i;
		for (i = 0; i < PIXELS; i++)
		{
			grbBuffer[REVERSE(i, PIXELS)] = i < pixelsCount
				? color
				: (i == pixelsCount
					? colorOfLast
					: KSLIGHT_TEMP_COLOR_NONE);
		}
	}
}
