﻿#ifndef __DELAY_TIMER_H__
#define __DELAY_TIMER_H__

#include "hardware.h"
#include "stdafx.h"
#include "stm32f0xx_tim.h"

#define DELAY_TIMER 		TIMER_FOR_DELAYS

PUBLIC void DelayTimer_Init(void);
PUBLIC void DelayTimer_ms(uint16_t delayTime);
PUBLIC void DelayTimer_us(uint16_t delayTime);
PUBLIC uint8_t DelayTimer_timerMs(uint16_t delayTime);

#define DELAYTIMER_US(DELAY) \
	do{\
		DELAY_TIMER->ARR = (DELAY) - 4;\
		DELAY_TIMER->SR = (uint16_t)~TIM_FLAG_Update;\
		DELAY_TIMER->CR1 |= TIM_CR1_CEN;\
		while (!(DELAY_TIMER->SR & TIM_FLAG_Update)) {}\
	}while(0)

//uint16_t d = (DELAY) << 3;
//if (d < 48)
//	break;

#endif /* __DELAY_TIMER_H__ */
