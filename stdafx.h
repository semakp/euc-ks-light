﻿#ifndef __STDAFX_H__
#define __STDAFX_H__


#define TRUE				(1)
#define FALSE				(0)

#define CHIPID_ISEQUAL(c1, c2)		(!memcmp((c1), (c2), 12))

#define GET_TS_HOUR(x)		((x%(24*3600))/3600)
#define GET_TS_MIN(x)		((x%3600)/60)
#define GET_TS_SEC(x)		((x%3600)%60)

#define SETBITS(x,y) 	(x)|=(y)
#define SETBIT(x,y) 	SETBITS((x),1<<(y))
#define CLRBITS(x,y)	(x)&=~(y)
#define CLRBIT(x,y) 	CLRBITS((x),1<<(y))

#define CHECK_BIT(x,bit)	((x) & (1<<(bit)))
#define CHECK_BITS(x,bits)	((x) & (bits))

#define CHECK_BIT60(x,bit)	(((bit) < 30) && CHECK_BIT(x##0, bit)) || (((bit) >= 30) && CHECK_BIT(x##1, (bit) - 30))

#define CHECK_FLAGINT_GT(flagint,intg)	((flagint) & ~((1 << (intg)) - 1))

#define MIN(x,y)		((x)<(y))?(x):(y)
#define MAX(x,y)		((x)>(y))?(x):(y)

#define HIGH(x)			((x)>>8)
#define LOW(x)			((x)&0xFF)

#define NO_OPTIMIZE		volatile
#define PRIVATE 		static
#define PUBLIC

#define ACTION(x) 		void (*x)(void)
#define ACTION1(x) 		void (*x)(void *)
#define ACTION_u8_u8(x) 	void (*x)(uint8_t , uint8_t )

#define ACTION_DATA(x)			void (*x)(uint8_t *, uint16_t )
#define ACTION_DATA_CB(x)		void (*x)(uint8_t *, uint16_t , ACTION_DATA(cb))
#define ACTION_HDL_DATA(x)		void (*x)(uint8_t, uint8_t *, uint16_t )
#define ACTION_DATA_ZLITE(x)	void (*x)(uint8_t, uint8_t, void *, void *, uint16_t )

#define ACTION_CALLBACK(x) 	void (*x)(uint16_t , uint32_t )
//typedef void actionCallback_t(uint8_t , uint32_t );

#define INTERVAL(HH,MM,SS)	((HH)*60*60 + (MM)*60 + (SS))
#define INTERVAL_MSEC(HH,MM,SS,MS)	((HH)*60*60*1000 + (MM)*60*1000 + (SS)*1000 + (MS))

#define SEC(ts)		((ts) % 60)
#define MINUTE(ts)	(((ts) % 3600) / 60)
#define HOUR(ts)	(((ts) % 86400) / 3600)
#define DATE(ts)	((ts) / 86400)
#define DAY(ts)		(DATE(ts) % 7)

#define CIRCLE_INCREMENT(value, maximum)		do{ \
	(value)++;\
	if((value) >= (maximum)){ \
		(value) = 0; \
	}} while(0);

#define CIRCLE_DECREMENT(value, maximum)		do{ \
	if((value) == 0){ \
		(value) = (maximum); \
	}else{\
		(value)--;\
	}} while(0);

#define CIRCLE_ADD(value, add, maximum)			do{ \
	if((value) >= ((maximum) - (add))){ \
		(value) = (value) - ((maximum) - (add)); \
	} else { \
		(value) += (add); \
	}} while(0);

#define CIRCLE_ADD_TO(result, value, add, maximum)			do{ \
	if((value) >= ((maximum) - (add))){ \
		(result) = (value) - ((maximum) - (add)); \
	} else { \
		(result) = (value) + (add); \
	}} while(0);

#define CIRCLE_FOR(iter, from, to, maximum)		for((iter) = (from); (iter) != (to); (iter) = ((iter) == ((maximum) - 1)? 0 : ((iter) + 1)))
#define CIRCLE_MEMCPY(dstCircle, dstOffset, src, len, circleSize) \
	do{ if((dstOffset) >= ((circleSize) - (len)){ \
		memcpy(&(dstCircle)[(dstOffset)], &(src)[0], (circleSize) - (dstOffset)); \
		memcpy(&(dstCircle)[0], &(src)[(circleSize) - (dstOffset)], (len) - ((circleSize) - (dstOffset))); \
	} else { \
		memcpy(&(dstCircle)[0], &(src)[0], (len)); \
	} }

#endif
