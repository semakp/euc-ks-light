﻿#ifndef __MEASURE_TIMER_H__
#define __MEASURE_TIMER_H__

#include "stdafx.h"
#include "stm32f0xx_tim.h"

#define MEASURE_TIMER_START(MT)	do{(MT)->SR = (uint16_t)~TIM_IT_Update;(MT)->CR1 |= TIM_CR1_CEN;}while(0)
#define MEASURE_TIMER_VALUE(MT)	((MT)->CNT)
#define MEASURE_TIMER_CANCEL(MT) do{(MT)->EGR = TIM_EventSource_Update;}while(0)
#define MEASURE_TIMER_STOP(MT,lvalue)	do{lvalue = (MT)->CNT;(MT)->EGR = TIM_EventSource_Update;}while(0)

PUBLIC void MeasureTimer_Init(TIM_TypeDef* TIMx, uint16_t prescaler);

#endif /* __MEASURE_TIMER_H__ */

