﻿#ifndef __WS2812_OUT__
#define __WS2812_OUT__

#include "stdafx.h"
#include "stm32f0xx.h"
#include "hardware.h"

#define WS2812OUT_TOTAL	4
#define WS2812OUT_FL	0
#define WS2812OUT_FR	1
#define WS2812OUT_BL	2
#define WS2812OUT_BR	3

PUBLIC void Ws2812Out_InitWithDma(void);
PUBLIC void Ws2812Out_DmaSend(uint8_t (*dmaBuffers)[WS2812OUT__DMA_BUFFER_SIZE]);
PUBLIC void Ws2812Out_FromBuffer(uint8_t *dmaBuffer, uint32_t *buffer, uint16_t bufSize);

#endif /* __WS2812_OUT__ */
