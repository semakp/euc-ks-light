﻿#ifndef __COMMON_H__
#define __COMMON_H__

#include <string.h>
#include "stm32f0xx.h"
#include "stdafx.h"

PUBLIC void memcpy_u16(uint8_t *target, uint16_t src);
PUBLIC void memcpy_u32(uint8_t *target, uint32_t src);
PUBLIC void memrcpy(void *dst, void *src, uint16_t len);
PUBLIC void memrcpy_u16(uint8_t *target, uint16_t src);
PUBLIC void memrcpy_u32(uint8_t *target, uint32_t src);
PUBLIC uint32_t memrget_u32(uint8_t *src);
PUBLIC uint16_t memrget_u16(uint8_t *src);
PUBLIC uint16_t memget_u16(uint8_t *src);
PUBLIC void delay_us(unsigned long n);
PUBLIC void delay_ms(unsigned long n);

PUBLIC void Common_sendStr(ACTION_DATA(send), char *str);
PUBLIC void Common_sendDec(ACTION_DATA(send), uint16_t num, uint8_t dec);
PUBLIC void Common_sendDec32(ACTION_DATA(send), uint32_t num, uint8_t dec, char space, uint8_t dot);
PUBLIC void Common_sendHex(ACTION_DATA(send), uint32_t num, uint8_t dec);

PUBLIC uint16_t Common_memFind(void *psrc, uint16_t srclen, void *pdst, uint16_t dstlen);
PUBLIC uint8_t Common_memContains(void *src, uint16_t srclen, void *dst, uint16_t dstlen);

PUBLIC uint8_t Common_Circle_IndexAddWillOverflow(uint16_t inx, uint16_t *outNextInx, uint16_t add, uint16_t maxInx, uint16_t circleSize);

#endif
